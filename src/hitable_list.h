#ifndef HITABLE_LIST_H
#define HITABLE_LIST_H

#include<stdbool.h>
#include "hitable.h"
#include "ray.h"
#include "AABB.h"

typedef struct
{
    char* listname;
    hitable** objects;
    unsigned int list_size;    
} hitable_list;

// constructor
hitable_list* hitable_list_new(char* listname);

int hitable_list_addObj(hitable_list* the_list, hitable* const newObj);

// destructor
void hitable_list_free(hitable_list* the_list);

// ray intersection code
bool hitable_list_hit(const hitable_list* list, const ray* r, const float t_min, const float t_max, hit_record* rec, unsigned int *seed);

// bounding box
bool hitable_list_create_aabb(hitable_list* the_list, float time0, float time1, aabb* output_box);

#endif
