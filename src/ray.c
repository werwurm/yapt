#include "ray.h"
#include "vec3.h"

point3 ray_origin(const ray self)
{
    return self.origin;
}

vec3 ray_direction(const ray self)
{
    return self.direction;
}

point3 ray_point_at_parameter(const ray self, float t)
{
    return (vec3){self.origin.e0 + self.direction.e0*t, self.origin.e1 + self.direction.e1*t, self.origin.e2 + self.direction.e2*t};
}
    
