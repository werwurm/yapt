// instancing and translation/rotation of objects

#ifndef INSTANCE_H
#define INSTANCE_H

#include"hitable.h"
#include<stdbool.h>
#include"vec3.h"

// class definitions
typedef struct
{
    hitable* obj;
    vec3 offset;
} instance_translate;

typedef enum
{
    X, Y, Z
} rotation_axis;

typedef struct
{
    hitable* obj;
    float sin_theta;
    float cos_theta;
    int hasbox;
    aabb bbox;
    rotation_axis axis;
} instance_rotate;

// constructors
hitable* instance_translate_init(hitable* obj, vec3 offset);
hitable* instance_rotate_init(hitable* obj, float angle, rotation_axis axis);

// bounding boxes
bool instance_translate_create_aabb(instance_translate* obj, float t0, float t1, aabb* the_box);
bool instance_rotate_create_aabb(instance_rotate* obj, aabb* the_box);

// destructors
void instance_translate_free(hitable* object);
void instance_rotate_free(hitable* object);

// ray intersection code
bool instance_translate_hit(instance_translate* obj, const ray* r, float tmin, float tmax, hit_record* rec, unsigned int *seed);
bool instance_rotate_hit(instance_rotate* obj, const ray* r, float tmin, float tmax, hit_record* rec, unsigned int *seed);
#endif  // INSTANCE_H

