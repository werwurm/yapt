#ifndef BVH_H
#define BVH_H

#include <stdbool.h>

#include"hitable.h"
#include"hitable_list.h"
#include"ray.h"

typedef struct
{
    hitable* left;
    hitable* right;
    
    aabb bounding_box;
} bvh_node;

// constructor
hitable* bvh_node_init(hitable_list* objects, float t0, float t1);
hitable* bvh_node_construction(hitable** src_objects, unsigned int src_objects_count, unsigned int start, unsigned int end, float t0, float t1);

// create bounding box
bool bvh_node_create_aabb(bvh_node* node, aabb* output_box);

// ray intersection code
bool bvh_node_hit(bvh_node* node, const ray* r, float t_min, float t_max, hit_record* rec, unsigned int *seed);

// box size comparison for all 3 axes
int box_x_compare(const void* a, const void* b);
int box_y_compare(const void* a, const void* b);
int box_z_compare(const void* a, const void* b);
#endif // BVH_H
