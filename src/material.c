#include<stdbool.h>
#include<stdio.h>
#include "vec3.h"
#include "hitable.h"
#include "ray.h"
#include "material.h"
#include "utils.h"
#include "texture.h"

// functions that are only accessible within this file
static void material_lambertian_free(material* mat);
static void material_dielectric_free(material* mat);
static void material_metal_free(material* mat);
static void material_light_free(material* mat);
static void material_isotropic_free(material* mat);

//#############################################################################
// constructors
//#############################################################################
// init generic abstract material
material* material_generic_new(material_type t)
{
    material* m = malloc(sizeof(material));
    m->type = t;

    return m;
}

// Init Lambertian
material* material_lambertian_new(texture* albedo)
{
    material* mat = (material*) material_generic_new(MATERIAL_LAMBERTIAN);
    material_lambertian* l = malloc(sizeof(material_lambertian));
    l->albedo = albedo;
    mat->mat = l;
    return mat;
}

// Init metal
material* material_metal_new(texture* albedo, float fuzz)
{
    material* mat = (material*) malloc(sizeof(material));
    mat->type = MATERIAL_METAL;
    material_metal* metal = (material_metal*) malloc(sizeof(material_metal));
    metal->albedo = albedo;
    metal->fuzz = fuzz;
    mat->mat = metal;
    return mat;
}

// Init dielectric
material* material_dielectric_new(texture* albedo, float ir)
{
    material* mat = (material*) malloc(sizeof(material));
    mat->type = MATERIAL_DIELECTRIC;
    material_dielectric* d = (material_dielectric*) malloc(sizeof(material_dielectric));
    d->albedo = albedo;
    d->ir = ir;
    mat->mat = d;
    return mat;
}

// init emissive material (by texture)
material* material_light_new(texture* emit)
{
    material* mat = (material*) malloc(sizeof(material)); 
    mat->type = MATERIAL_LIGHT;
    
    material_light* l = (material_light*) malloc(sizeof(material_light));
    l->emit = emit;
    
    mat->mat = l;
    return mat;
}

// init emissive material (by color)
material* material_light_new_color(color col)
{
    material* mat = (material*) malloc(sizeof(material)); 
    mat->type = MATERIAL_LIGHT;
    
    material_light* l = (material_light*) malloc(sizeof(material_light));
    l->emit = texture_solid_color_init(col);
    
    mat->mat = l;
    return mat;    
}

// init isotropic material (smoke, dust, fog)
material* material_isotropic_new(texture* albedo)
{
    material* mat = (material*) malloc(sizeof(material));
    mat->type = MATERIAL_ISOTROPIC;
    
    material_isotropic* i = (material_isotropic*) malloc(sizeof(material_isotropic));
    i->albedo = albedo;
    
    mat->mat = i;
    return mat;
}


// init list of materials
material_list* material_list_new(char* listname)
{
    material_list* newlist=NULL;
    
    //allocate memory for list itself
    newlist = (material_list*) malloc(sizeof(material_list));
    
    // allocate memory for ptrs to materials
    newlist->mat = (material**) malloc(sizeof(material*));
    
    // newly created list is empty
    newlist->list_size = 0;
    
    newlist->listname = listname;
    
    return newlist;   
}

// add material to material list
int material_list_addMat(material_list* the_list, material* const newMat)
{  
    if((newMat->type == MATERIAL_LAMBERTIAN) ||
        (newMat->type == MATERIAL_DIELECTRIC) ||
        (newMat->type == MATERIAL_METAL) ||
        (newMat->type == MATERIAL_LIGHT) ||
        (newMat->type == MATERIAL_ISOTROPIC)) 
    {
        // resize list and add texture
        the_list->mat = (material**) realloc(the_list->mat, sizeof(material**) * (the_list->list_size + 1));   
        the_list->mat[the_list->list_size] = newMat;
        the_list->list_size++;        
    }
    else
    {
        printf("Adding materials of type %d not implemented. Aborting!", newMat->type);
        exit(EXIT_FAILURE);
    }
    return (the_list->list_size - 1);   // return index in list
}

//#############################################################################
// destructors
//#############################################################################
// generic free material, free textures and material
void material_free(material* mat)
{
    if(mat->type == MATERIAL_LAMBERTIAN)
    {   
        material_lambertian_free(mat);
    }
    else if(mat->type == MATERIAL_DIELECTRIC)
    {
        material_dielectric_free(mat);
    }
    else if(mat->type == MATERIAL_METAL)
    {
        material_metal_free(mat);
    }
    else if(mat->type == MATERIAL_LIGHT)
    {
        material_light_free(mat);
    }
    else if(mat->type == MATERIAL_ISOTROPIC)
    {
        material_isotropic_free(mat);
    }
    else
    {
        printf("\n freeing material objects of type %d not implemented. Aborting!", mat->type);
        exit(EXIT_FAILURE);
    }
}

// free lambertian material
static void material_lambertian_free(material* mat)
{
    free(mat->mat);
    free(mat);
}

// free dieelectric material
static void material_dielectric_free(material* mat)
{
    free(mat->mat);
    free(mat);
}

// free metal material
static void material_metal_free(material* mat)
{
    free(mat->mat);
    free(mat);
}

// free light emitting material
static void material_light_free(material* mat)
{
    free(mat->mat);
    free(mat);
}

// free isotropic material
static void material_isotropic_free(material* mat)
{
    free(mat->mat);
    free(mat);
}


void material_list_free(material_list* the_list)
{
    printf("Freeing %d materials in list %s\n", the_list->list_size, the_list->listname);
    fflush(stdout);
    for(unsigned int i=0; i<the_list->list_size; i++)
    {
        material_free(the_list->mat[i]);
    }
    
    free(the_list->mat);
    free(the_list);    
}

//#############################################################################
// Material Scatter Code
//#############################################################################
// needed by scatter_metal function
vec3 reflect(const vec3 *v, const vec3 *n)
{
    float vecdot = vec3_dot(*v, *n);
    vecdot = 2.0f * vecdot;
    vec3 tmp = vec3_skalarMult(*n, vecdot);
    return vec3_sub(*v, tmp);
} 

// needed by dielectrics 
bool refract(const vec3 *v, const vec3 *n, float ni_over_nt, vec3 *refracted)
{
    vec3 uv = vec3_unit_vector(*v);
    float dt = vec3_dot(uv,*n);
    float discriminant = 1.0f - ni_over_nt * ni_over_nt * (1- dt*dt);
    if(discriminant > 0)
    {
        vec3 tmp = vec3_skalarMult(*n, dt);
        tmp = vec3_sub(uv, tmp);
        tmp = vec3_skalarMult(tmp, ni_over_nt);
        float var = sqrtf(discriminant);
        *refracted = vec3_skalarMult(*n, var);
        *refracted = vec3_sub(tmp, *refracted);
        return true;
    }
    return false;    
}

// needed by dielectrics
// polynomial approximation for angle-dependent reflectivity
float schlick(float cosine, float ref_idx) 
{
    float r0 = (1-ref_idx) / (1+ref_idx);
    r0 = r0*r0;
    return (r0 + (1-r0)*pow((1 - cosine),5));
}

// Scatter function for lambertian material
static bool material_lambertian_scatter(material_lambertian* mat, const ray* r, hit_record* rec, color* attenuation, ray* scattered, unsigned int *seed)
{
//     (void)r;   // supress compiler warning about unused parameter
    
    //Scatter the ray following the normal of the hit, and slightly randomize it
    vec3 scatter_direction = vec3_add(rec->normal, random_in_unit_sphere_multithread(seed));
    
    //Check for near zero scattered directions
    if (vec3_is_near_zero(&scatter_direction)){scatter_direction = rec->normal;}
    
    *scattered = (ray){rec->p, scatter_direction, r->time};
    
    *attenuation = texture_value(mat->albedo, rec->u, rec->v, &rec->p);
    
    return true;
}

// Scatter function for metal material
static bool material_metal_scatter(material_metal* mat, const ray* r, hit_record* rec, color* attenuation, ray* scattered, unsigned int *seed)
{
    //Scatter the ray reflecting it using the normal as bisector
    vec3 tmpvec = vec3_unit_vector(r->direction);
    vec3 reflected = reflect(&tmpvec, &rec->normal);
    scattered->origin = rec->p;
    
    vec3 fuzzy_scatter = vec3_skalarMult(random_in_unit_sphere_multithread(seed), mat->fuzz); // for metals, 1: perfect reflection, 0: absorbation
    scattered->direction = vec3_add(reflected, fuzzy_scatter);
    scattered->time = r->time;
    *attenuation = texture_value(mat->albedo, rec->u, rec->v, &rec->p);
    return (vec3_dot(scattered->direction, rec->normal) > 0);   
}

// Scatter function for dielectric material
static bool material_dielectric_scatter(material_dielectric* mat, const ray* r, hit_record* rec, color* attenuation, ray* scattered, unsigned int *seed)
{
    float refraction_ratio = rec->front_face ? (1.0/mat->ir) : mat->ir;
    vec3 unit_dir = vec3_unit_vector(r->direction);
    
    float cos_theta = fmin(vec3_dot(vec3_flip(&unit_dir), rec->normal), 1.0);
    float sin_theta = sqrtf(1.0 - cos_theta * cos_theta);
    
    bool cannot_refract = refraction_ratio * sin_theta > 1.0;
    
    vec3 direction;
    
    if(cannot_refract || schlick(cos_theta, refraction_ratio) > random_float_multithread(seed))
    {
        direction = reflect(&unit_dir, &rec->normal);   // total internal reflection
    }
    else
    {
        refract(&unit_dir, &rec->normal, refraction_ratio, &direction); // can refract
    }

    *scattered = (ray){rec->p, direction, r->time};
    *attenuation = texture_value(mat->albedo, rec->u, rec->v, &rec->p); 
    return true;
}

static bool material_isotropic_scatter(material_isotropic* mat, const ray* r, hit_record* rec, color* attenuation, ray* scattered, unsigned int *seed)
{
    *scattered = (ray) {rec->p, random_in_unit_sphere_multithread(seed), r->time};
    *attenuation = texture_value(mat->albedo, rec->u, rec->v, &rec->p);
    return true;    
}

// Generic material function, calls the right scatter function based on material type
bool material_scatter(material* mat, const ray* r, hit_record* rec, color* attenuation, ray* scattered, unsigned int *seed)
{
    switch(mat->type)
    {
        case MATERIAL_LAMBERTIAN:
            return material_lambertian_scatter((material_lambertian*)mat->mat, r, rec, attenuation, scattered, seed);
            break;
        case MATERIAL_DIELECTRIC:
            return material_dielectric_scatter((material_dielectric*)mat->mat, r, rec, attenuation, scattered, seed);
            break;
        case MATERIAL_METAL:
            return material_metal_scatter((material_metal*)mat->mat, r, rec, attenuation, scattered, seed);
            break;
        case MATERIAL_LIGHT:    // light sources do not scatter incoming light
            return false;
            break; 
        case MATERIAL_ISOTROPIC:
            return material_isotropic_scatter((material_isotropic*) mat->mat, r, rec, attenuation, scattered, seed);
            break;
        default:
            printf("No scatter function implemented for material type %d\n", mat->type); 
            exit(EXIT_FAILURE);
            break;
    }    
//     if(mat->type == MATERIAL_LAMBERTIAN)
//     {
//         return material_lambertian_scatter((material_lambertian*)mat->mat, r, rec, attenuation, scattered);
//     }
//     else if(mat->type == MATERIAL_DIELECTRIC)
//     {
//         return material_dielectric_scatter((material_dielectric*)mat->mat, r, rec, attenuation, scattered);
//     }
//     else if(mat->type == MATERIAL_METAL)
//     {
//         return material_metal_scatter((material_metal*)mat->mat, r, rec, attenuation, scattered);
//     }
//     else if(mat->type == MATERIAL_LIGHT)
//     {
//         return false;   // light sources do not scatter incoming light
//     }
//     else if(mat->type == MATERIAL_ISOTROPIC)
//     {
//         return material_isotropic_scatter((material_isotropic*) mat->mat, r, rec, attenuation, scattered);
//     }
//     else
//     {
//         printf("No scatter function implemented for material type %d\n", mat->type); 
//         exit(EXIT_FAILURE);
//     }
}

// emitted light from light source
static color material_light_emitted(material_light* mat, float u, float v, point3 p)
{
    return texture_value(mat->emit, u, v, &p);
}

// generic emission function
color material_emitted(material* mat, float u, float v, point3 p)
{
    // calculate light emission only for lights
    if(mat->type == MATERIAL_LIGHT)
    {
        return material_light_emitted((material_light*)mat->mat, u, v, p);
    }
    else    // return black for all other materials
    {
        return (color){0,0,0};
    }
}


// bool scatter_phong(const void *r_in, const void *rec, void *attenuation, void *scattered)
// {
//     // typecast arguments
//     const hit_record *record = (const hit_record*) rec;
//     ray *scatter = (ray *) scattered;
//     vec3 *attenue = (vec3 *) attenuation;
//     const ray *ray_in = (const ray*) r_in;
//     
//     vec3 target = vec3_add(record->p, record->normal);
//     vec3 randomvec = random_in_unit_sphere();
//     target = vec3_add(target, randomvec);
//     vec3 tmpvec = vec3_sub(target, record->p);
//     scatter->A = record->p;
//     scatter->B = tmpvec;
//   
//     float ambient_factor = 0.2;
//     float specular_power = 16;
//     
//     vec3 ambient = vec3_skalarMult(record->mat.phong.albedo, ambient_factor);
//    
//     vec3 tmpvec2 = vec3_unit_vector(ray_in->B);
//     vec3 reflected = reflect(&tmpvec2, &record->normal);
//     float diffuse_idx = vec3_dot(record->normal, reflected);
//     if(diffuse_idx>1.0)
//          diffuse_idx = 1.0;
//     vec3 diffuse =  vec3_skalarMult(record->mat.phong.albedo, diffuse_idx);
//     
//     *attenue = vec3_add(ambient, diffuse);
//     
//     vec3 tmpvec3 = vec3_skalarMult(ray_in->B, -1);
//     float specular_idx = vec3_dot(reflected, tmpvec3);
//     specular_idx = pow(specular_idx, specular_power);
//     if(specular_idx > 1.0)
//         specular_idx = 1.0;
//     vec3 specular = vec3_skalarMult(record->mat.phong.albedo, specular_idx);
// 
//     *attenue = vec3_add(*attenue, specular);
//    
//     return true;
// }


