#include<stdio.h>
#include "hitable.h"
#include "sphere.h"
#include "AABB.h"
#include "BVH.h"
#include "aarect.h"
#include "instance.h"
#include "constant_medium.h"
#include "box.h"


// generic "constructor" for all hitable objects
hitable* hitable_generic_init(hitable_type type, void* object)
{
    hitable* new = (hitable*) malloc(sizeof(hitable));
    new->type = type;
    new->obj = object;
    
    return new;    
}

// generic "destructor" for all hitable objects
void hitable_generic_free(hitable* object)
{
    if(object->type == HITABLE_SPHERE)
    {
        sphere_free(object);
    }
    else if(object->type == HITABLE_MOVING_SPHERE)
    {
        moving_sphere_free(object);
    }
    else if(object->type == HITABLE_RECT_XY)
    {
        xy_rect_free(object);
    }
    else if(object->type == HITABLE_RECT_XZ)
    {
        xz_rect_free(object);
    }
    else if(object->type == HITABLE_RECT_YZ)
    {
        yz_rect_free(object);
    }
    else if(object->type == HITABLE_BOX)
    {
        box_free(object);
    }
    else if(object->type == HITABLE_TRANSLATE)
    {
        instance_translate_free(object);
    }
    else if(object->type == HITABLE_ROTATE)
    {
        instance_rotate_free(object);
    }
    else if(object->type == HITABLE_CONSTANT_MEDIUM)
    {
        constant_medium_free(object);
    }
    else
    {
        printf("\nfreeing memory for object type %d not implemented. Aborting!\n", object->type);
        exit(EXIT_FAILURE);
    }
}

// generic function to create bounding_box
int hitable_create_aabb(hitable* the_object, float t0, float t1, aabb* bounding_box)
{
    if(the_object->type == HITABLE_SPHERE)
    {
        return sphere_create_aabb((sphere*)the_object->obj, bounding_box);
    }
    else if(the_object->type == HITABLE_MOVING_SPHERE)
    {
        return moving_sphere_create_aabb((moving_sphere*)the_object->obj, t0, t1, bounding_box);     
    }
    else if(the_object->type == HITABLE_BOX)
    {
        return box_create_aabb((box*) the_object->obj, bounding_box);
    }
    else if(the_object->type == HITABLE_TRANSLATE)
    {
        return instance_translate_create_aabb((instance_translate*) the_object->obj, t0, t1, bounding_box);
    }
    else if(the_object->type == HITABLE_ROTATE)
    {
        return instance_rotate_create_aabb((instance_rotate*) the_object->obj, bounding_box);
    }
    else if(the_object->type == HITABLE_CONSTANT_MEDIUM)
    {
        return constant_medium_create_aabb((constant_medium*) the_object->obj, t0, t1, bounding_box);
    }
    else if(the_object->type == HITABLE_BVH_NODE)
    {
        return bvh_node_create_aabb((bvh_node*) the_object->obj, bounding_box);
    }
    else 
    {
        printf("\n no bounding box generation implemented for type %d. Aborting!\n", the_object->type);
        exit(EXIT_FAILURE);
    }
}

// generic object hit test function
bool hitable_object_hit(const hitable *obj, const ray *r, const float t_min, const float t_max, hit_record *rec, unsigned int *seed)
{
    bool hit = false;
    switch(obj->type)
    {
        case HITABLE_SPHERE:
            return sphere_hit((sphere*)obj->obj, r, t_min, t_max, rec);
            break;
        case HITABLE_MOVING_SPHERE:
            return moving_sphere_hit((moving_sphere*)obj->obj, r, t_min, t_max, rec);
            break;
        case HITABLE_BVH_NODE:
            return bvh_node_hit((bvh_node*) obj->obj, r, t_min, t_max, rec, seed);
            break;
        case HITABLE_RECT_XY:
            return xy_rect_hit((xy_rect*) obj->obj, r, t_min, t_max, rec);
            break;
        case HITABLE_RECT_XZ:
            return xz_rect_hit((xz_rect*) obj->obj, r, t_min, t_max, rec);
            break;
        case HITABLE_RECT_YZ:
            return yz_rect_hit((yz_rect*) obj->obj, r, t_min, t_max, rec);
            break;
        case HITABLE_BOX:
            return box_hit((box*)obj->obj, r, t_min, t_max, rec, seed);
            break;
        case HITABLE_TRANSLATE:
            return instance_translate_hit((instance_translate*) obj->obj, r, t_min, t_max, rec, seed);
            break;
        case HITABLE_ROTATE:
            return instance_rotate_hit((instance_rotate*) obj->obj, r, t_min, t_max, rec, seed);
            break;
        case HITABLE_CONSTANT_MEDIUM:
            return constant_medium_hit((constant_medium*) obj->obj, r, t_min, t_max, rec, seed);
            break;
        default: 
            printf("\n no intersection code for type %d implemented. Aborting!\n", obj->type);
            exit(EXIT_FAILURE);
            break;
    }       
    return hit;
}


