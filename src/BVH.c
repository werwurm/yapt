#include<string.h>
#include<stdio.h>
#include"BVH.h"
#include"AABB.h"
#include"utils.h"

// constructor, the root of a BVH tree
hitable* bvh_node_init(hitable_list* objects, float t0, float t1)
{
    return bvh_node_construction(objects->objects, objects->list_size, 0, objects->list_size, t0, t1); 
}


hitable* bvh_node_construction(hitable** src_objects, unsigned int src_objects_count, unsigned int start, unsigned int end, float t0, float t1)
{
    bvh_node* new_node= (bvh_node*) malloc(sizeof(bvh_node));
    
    //Create copy of src_object to be able to sort it without messing with the original order
    hitable** objects = malloc(sizeof(hitable*)*src_objects_count);
    memcpy(objects, src_objects, sizeof(hitable*)*src_objects_count);

    //Select a random axis
    unsigned int raxis = random_float();
    int(*comparator)(const void*, const void*);
    if (raxis < 0.33)
    {
        comparator = &box_x_compare;
    }
    else if (raxis < 0.66)
    {
        comparator = &box_y_compare;
    }
    else
    {
        comparator = &box_z_compare;
    }

    //Create leafs based on how many items i have to organize
    int object_span = end-start;
    if(object_span == 1)
    {
        //Only one: make both leafs the same object
        new_node->left = objects[start];
        new_node->right = new_node->left;
    }
    else if (object_span == 2)
    {
        //Two items, sort those and assign
        int ind1, ind2;
        if(comparator(&objects[start], &objects[start+1])){ind1 = start; ind2 = start+1;
        }else{ind1 = start+1; ind2 = start;}
        new_node->left = objects[ind1];
        new_node->right = objects[ind2];
    }
    else
    {
        //Multiple items: recursively sort, divide et impera
        qsort(objects, src_objects_count, sizeof(objects[0]), comparator);
        int mid = start + object_span/2;
        new_node->left  = bvh_node_construction(src_objects, src_objects_count, start, mid, t0, t1);
        new_node->right = bvh_node_construction(src_objects, src_objects_count, mid, end, t0, t1);
    }

    //Make the two leafs AABB
    aabb box_left, box_right;
    if(!hitable_create_aabb(new_node->left, t0, t1, &box_left) || !hitable_create_aabb(new_node->right, t0, t1, &box_right))
    {
        printf("Failed to get bounding box in bvh_node constructor.\n"); 
        fflush(stdout); 
        return NULL;
    }

    //Make the current AABB
    new_node->bounding_box = create_surrounding_box(box_left, box_right);

    //Free the temporary object copy
    free(objects);

    //Make generic and return (NOTE: we do not add it to the current hittable_list!)
    hitable* h = malloc(sizeof(hitable));
    h->type = HITABLE_BVH_NODE;
    h->obj = new_node;
    return h;
//     hitable* newobject = hitable_generic_init(HITABLE_BVH_NODE, new_node);
    
//     return newobject;
}

// create bounding box
bool bvh_node_create_aabb(bvh_node* node, aabb* output_box)
{
    *output_box = node->bounding_box;
    return true;
}

// generic box comparison function, needed for qsort()
inline static bool box_compare(hitable* a, hitable* b, int axis)
{
    aabb box_a, box_b;
    if(!hitable_create_aabb(a, 0, 0, &box_a) || !hitable_create_aabb(b, 0, 0, &box_b))
    {
        printf("No bounding box in bvh_node constructor.\n"); 
        fflush(stdout);
        return false;
    }
    if (axis==0)
    {
        return (box_a.minimum.e0 < box_b.minimum.e0);
    }
    else if (axis==1)
    { 
        return (box_a.minimum.e1 < box_b.minimum.e1);
    }
    else
    {
        return (box_a.minimum.e2 < box_b.minimum.e2);
    }
    
}

// separate boxsize comparison functions for all 3 axes
int box_x_compare(const void* a, const void* b)
{
    return box_compare(*((hitable**)a), *((hitable**)b), 0);
}
int box_y_compare(const void* a, const void* b)
{
    return box_compare(*((hitable**)a), *((hitable**)b), 1);
}
int box_z_compare(const void* a, const void* b)
{
    return box_compare(*((hitable**)a), *((hitable**)b), 2);
}


// ray intersection code
bool bvh_node_hit(bvh_node* node, const ray* r, float t_min, float t_max, hit_record* rec, unsigned int *seed)
{
    if(aabb_hit((&node->bounding_box), r, t_min, t_max))
    {
        return false;
    }
    bool hit_left = hitable_object_hit(node->left, r, t_min, t_max, rec, seed);
    bool hit_right = hitable_object_hit(node->right, r, t_min, t_max, rec, seed);
    
    return (hit_left || hit_right);
}
