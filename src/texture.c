#include <stdio.h>
#include <math.h>
#include "texture.h"
#include "image.h"

//#############################################################################
// constructors
//#############################################################################
// generic texture object
texture* texture_generic_init(texture_type type, void* obj)
{
    texture* new = (texture*) malloc(sizeof(texture));
    new->type = type;
    new->obj = obj;
    
    return new;
}

// create texture from solid color
texture* texture_solid_color_init(color col)
{
    texture_solid_color* tex = (texture_solid_color*) malloc(sizeof(texture_solid_color));
    
    tex->color_value = col;
    texture* new_tex = texture_generic_init(TEXTURE_SOLID, tex);
    return new_tex;
}

// create texture from solid color with r,g,b
texture* texture_solid_color_init_rgb(float r, float g, float b)
{
    texture_solid_color* tex = (texture_solid_color*) malloc(sizeof(texture_solid_color));
    
    tex->color_value = (color){r,g,b};
    texture* new_tex = texture_generic_init(TEXTURE_SOLID, tex);
    return new_tex;   
}

// create checker texture from 2 textures
texture* texture_checker_init(texture* odd, texture* even)
{
    texture_checker* tex = (texture_checker*) malloc(sizeof(texture_checker));
    tex->odd = odd;
    tex->even = even;
    
    texture* new_tex = texture_generic_init(TEXTURE_CHECKER, tex);
    
    return new_tex;
}

// create checker texture from 2 colors
texture* texture_checker_init_byCol(color odd, color even)
{
    texture* newOdd = texture_solid_color_init(odd);
    texture* newEven = texture_solid_color_init(even);
    
    texture* new_tex = texture_checker_init(newOdd,newEven);
    
    return new_tex;
}

// create perlin noise texture
texture* texture_noise_init(color col, float scale)
{
    texture_noise* perlin_noise = (texture_noise*) malloc(sizeof(texture_noise));
    perlin* p = perlin_new();
    
    perlin_noise->noise = p;
    perlin_noise->scale = scale;
    
    perlin_noise->color_value = col; 
    
    texture* new_tex = texture_generic_init(TEXTURE_NOISE, perlin_noise);
    
    return new_tex;    
}

texture* texture_image_init(char* filename)
{
    texture_image* tex = (texture_image*) malloc(sizeof(texture_image));
    int comp = 3;
    tex->data = load_image(filename, &tex->width, &tex->height, &comp);

    if(tex->data == NULL)
    {
        printf("Couldn't load texture file %s. Aborting!", filename);
        exit(EXIT_FAILURE);
    }
    
    tex->bytes_per_scanline = 3 * tex->width;
    
    texture* new_tex = texture_generic_init(TEXTURE_IMAGE, tex);
        
    return new_tex;
}

texture_list* texture_list_new(char* listname)
{
    texture_list* newlist=NULL;
    
    //allocate memory for list itself
    newlist = (texture_list*) malloc(sizeof(texture_list));
    
    // allocate memory for ptrs to textures
    newlist->tex = (texture**) malloc(sizeof(texture*));
    
    // newly created list is empty
    newlist->list_size = 0;
    
    newlist->listname = listname;
    
    return newlist;   
}

//#############################################################################
// destructors
//#############################################################################

// generic texture destructor wrapper
void texture_generic_free(texture* tex)
{
    if(tex->type == TEXTURE_SOLID)
    {
        texture_solid_color_free((texture_solid_color*) tex->obj);
    }
    else if(tex->type == TEXTURE_CHECKER)
    {
        texture_checker_free((texture_checker*) tex->obj);
    }
    else if(tex->type == TEXTURE_NOISE)
    {
        texture_noise_free((texture_noise*) tex->obj);
    }
    else if(tex->type == TEXTURE_IMAGE)
    {
        texture_image_free((texture_image*) tex->obj);
    }
    else
    {
        printf("Freeing textures of type %d not implemented. Aborting!", tex->type);
    }
    
    free(tex);
}

void texture_solid_color_free(texture_solid_color* tex)
{
    free(tex);
}

void texture_checker_free(texture_checker* tex)
{
    // checker textures can be any texture, so call generic destructor again
    texture_generic_free(tex->even);
    texture_generic_free(tex->odd);
    free(tex);
}

void texture_noise_free(texture_noise* tex)
{
    perlin_free(tex->noise);
    free(tex);
}

void texture_image_free(texture_image* tex)
{
    free(tex->data);
    free(tex);
}

void texture_list_free(texture_list* the_list)
{
    printf("Freeing %d textures in list %s\n", the_list->list_size, the_list->listname);
    fflush(stdout);
    for(unsigned int i=0; i<the_list->list_size; i++)
    {
        texture_generic_free(the_list->tex[i]);
    }
    
    free(the_list->tex);
    free(the_list);    
}

//#############################################################################
// Color calculation for different textures
//#############################################################################

// generic texture calculation wrapper
color texture_value(texture* tex, float u, float v, const point3* p)
{
    if(tex->type == TEXTURE_SOLID)
    {
        return texture_value_solid_color((texture_solid_color*)tex->obj);
    }
    else if(tex->type == TEXTURE_CHECKER)
    {
        return texture_value_checker((texture_checker*) tex->obj, u,v, p);
    }
    else if(tex->type == TEXTURE_NOISE)
    {
        return texture_value_noise((texture_noise*) tex->obj, p);
    }
    else if(tex->type == TEXTURE_IMAGE)
    {
        return texture_value_image((texture_image*) tex->obj, u, v);
    }
    else
    {
        printf("\ntexture_value calculation not implemented for type %d\n", tex->type);
        fflush(stdout);
        exit(EXIT_FAILURE);
    }
}

// solid color texture
color texture_value_solid_color(texture_solid_color* tex)
{
    return tex->color_value;    // dead simple :D
}

// checker texture
color texture_value_checker(texture_checker* tex, float u, float v, const point3* p)
{
    float sines = sinf(10*p->e0) * sinf(10*p->e1) * sinf(10*p->e2);
    return (sines<0) ? texture_value(tex->odd, u, v, p) : texture_value(tex->even, u,v,p);
}

// noise texture
color texture_value_noise(texture_noise* tex, const point3* p)
{   
    float turbulence = perlin_turbulence(tex->noise, *p, 7);
    float color_gradient = 1 + sin(tex->scale * p->e2 + 10* turbulence);
    
    return vec3_skalarMult(tex->color_value, 0.5 * color_gradient);
}

// image texture
color texture_value_image(texture_image* tex, float u, float v)
{
    if(tex->data == NULL)       // without valid texture data, return CYAN as debugging aid
    {
        return (color){0,1,1};
    }
    
    // clamp texture coordinates to [0,1] x [1,0]
    float uu = clamp(u, 0.0, 1.0);
    float vv = 1.0 - clamp(v, 0.0, 1.0);    // flip V to image coordinates
    
    int i = uu * tex->width;
    int j = vv * tex->height;
    
    // clamp integer mapping, since actual coordinates should be less than 1.0
    if(i >= tex->width)
        i = tex->width - 1;
    if(j >= tex->height)
        j = tex->height - 1;
    
    float color_scale = 1.0 / 255.0;
    
    // find the corresponding pixel
    unsigned char* pixel = tex->data + j*tex->bytes_per_scanline + i*3;
    
    return (color){color_scale * pixel[0], color_scale * pixel[1], color_scale* pixel[2]};
}



// add existing texture to texture_list, returns index of added texture in list
int texture_list_addTex(texture_list* the_list, texture* const newTex)
{  
    if((newTex->type == TEXTURE_SOLID) ||
        (newTex->type == TEXTURE_CHECKER) ||
        (newTex->type == TEXTURE_NOISE) ||
        (newTex->type == TEXTURE_IMAGE)) 
    {
        // resize list and add texture
        the_list->tex = (texture**) realloc(the_list->tex, sizeof(texture**) * (the_list->list_size + 1));   
        the_list->tex[the_list->list_size] = newTex;
        the_list->list_size++;        
    }
    else
    {
        printf("Adding textures of type %d not implemented. Aborting!", newTex->type);
        exit(EXIT_FAILURE);
    }
    return (the_list->list_size - 1);   // return index in list
}
