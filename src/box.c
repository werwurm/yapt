#include<stdio.h>
#include<stdbool.h>

#include "box.h"
#include "aarect.h"
#include "hitable.h"
#include "hitable_list.h"
#include "vec3.h"

// constructor
hitable* box_init(point3 p0, point3 p1, material* mat)
{
    box* newbox = (box*) malloc(sizeof(box));
    
    newbox->box_min = p0;
    newbox->box_max = p1;
    
    // create 6 sides
    newbox->sides = hitable_list_new("box sides");
    hitable_list_addObj(newbox->sides, xy_rect_init(p0.e0, p1.e0, p0.e1, p1.e1, p1.e2,mat));
    hitable_list_addObj(newbox->sides, xy_rect_init(p0.e0, p1.e0, p0.e1, p1.e1, p0.e2,mat));
    
    hitable_list_addObj(newbox->sides, xz_rect_init(p0.e0, p1.e0, p0.e2, p1.e2, p1.e1, mat));
    hitable_list_addObj(newbox->sides, xz_rect_init(p0.e0, p1.e0, p0.e2, p1.e2, p0.e1, mat));
    
    hitable_list_addObj(newbox->sides, yz_rect_init(p0.e1, p1.e1, p0.e2, p1.e2, p1.e0, mat));
    hitable_list_addObj(newbox->sides, yz_rect_init(p0.e1, p1.e1, p0.e2, p1.e2, p0.e0, mat));
    
    // create generic object
    hitable* object = hitable_generic_init(HITABLE_BOX, newbox);
    return object;
}

// destructor
void box_free(hitable* object)
{
    if(object->type == HITABLE_BOX)
    {
        // upcast object to box type
        box* tempbox = object->obj;
        
        hitable_list_free(tempbox->sides);
        free(tempbox);
        free(object);
    }
    else
    {
        printf("\ncalling box_free() for object of type %d. Aborting now\n", object->type);
        exit(EXIT_FAILURE);
    }    
}

// bounding box is identical to box
bool box_create_aabb(box* the_box, aabb* output_box)
{
    output_box = aabb_init(the_box->box_min, the_box->box_max);
    return true;
}


// ray intersection code for boxes
bool box_hit(const box* the_box, const ray *r, float t_min, float t_max, hit_record *rec, unsigned int *seed)
{
    return hitable_list_hit(the_box->sides, r, t_min, t_max, rec, seed);
}
