#include<stdbool.h>
#include<string.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "inc/stb_image_write.h"

#define STB_IMAGE_IMPLEMENTATION
#include "inc/stb_image.h"

#include "image.h"

// Check if a string "str" ends with a substring "ends"
static inline bool check_file_format(const char *filename, const char *extension) 
{
    char *pos = strrchr(filename, '.');
    if(pos == NULL)
    {
        return false;
    }
    else
    {
        return !strcmp(pos, extension);
    }
}

// wrapper for various stbi_write_* functions
void save_image(char *filename, int w, int h, int comp, const void *data)
{
    // flip image
    stbi_flip_vertically_on_write(1);    
    if(check_file_format(filename, ".png") || check_file_format(filename, ".PNG"))
    {
        stbi_write_png(filename, w, h, comp, data, w*comp);        
    }
    else if(check_file_format(filename, ".jpg") || check_file_format(filename, ".JPG") 
        || check_file_format(filename, ".jepg") || check_file_format(filename, ".JPEG"))
    {
        stbi_write_jpg(filename, w, h, comp, data, 100);
    }
    else if(check_file_format(filename, ".bmp") || check_file_format(filename, ".BMP"))
    {
        stbi_write_bmp(filename, w, h, comp, data);
    }
    else if(check_file_format(filename, ".tga") || check_file_format(filename, ".TGA"))
    {
        stbi_write_tga(filename, w, h, comp, data);
    }
    else    // default fallback: png
    {
        char extension[4]= ".png";
        strncat(filename, extension, 4);
        stbi_write_png(filename, w, h, comp, data, w*comp);  
    }
}

// wrapper stbi_load
unsigned char* load_image(char *filename, int *w, int *h, int *comp)
{
    return stbi_load(filename, w, h, comp, *comp);
}
