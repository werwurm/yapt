#ifndef IMAGE_H
#define IMAGE_H

void save_image(char *filename, int w, int h, int comp, const void *data);
unsigned char* load_image(char *filename, int *w, int *h, int *comp);

#endif // IMAGE_H
