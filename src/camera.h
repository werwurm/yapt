#ifndef CAMERA_H
#define CAMERA_H

#include "vec3.h"
#include "ray.h"

typedef struct
{
    vec3 origin;
    vec3 lower_left_corner;
    vec3 horizontal;
    vec3 vertical;
    
    vec3 u;
    vec3 v;
    vec3 w;
    float lens_radius;
    
    float time0;    // shutter open
    float time1;    // shutter close
} camera;

ray camera_get_ray(camera *the_cam, float s, float t, unsigned int *seed);
void camera_init(camera *the_cam, vec3 lookfrom, vec3 lookat, vec3 vecup, float vfov, float aspect, float aperture, float focus_dist, float time0, float time1);

#endif
