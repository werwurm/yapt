#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include <omp.h>

#include "aarect.h"
#include "box.h"
#include "BVH.h"
#include "camera.h"
#include "constant_medium.h"
#include "image.h"
#include "instance.h"
#include "vec3.h"
#include "sphere.h"
#include "material.h"
#include "hitable_list.h"
#include "hitable.h"
#include "utils.h"
#include "vec3.h"

void add_pixel_to_imagebuffer(unsigned char* pixels, const color* pixel_color, const unsigned int samples_per_pixel, const unsigned int index)
{
    float r = pixel_color->e0; 
    float g = pixel_color->e1; 
    float b = pixel_color->e2;
    
    // divide by nr of samples and adjust gamma
    float scale = 1.0 / (float)samples_per_pixel;
    
    // translate to [0,255] rgb values
    r = sqrt(scale * r); 
    g = sqrt(scale * g); 
    b = sqrt(scale * b);
    
    pixels[index]   = (unsigned char)(256*clamp(r, 0.0, 0.999));    // red
    pixels[index+1] = (unsigned char)(256*clamp(g, 0.0, 0.999));    // green
    pixels[index+2] = (unsigned char)(256*clamp(b, 0.0, 0.999));    // blue    
}

color ray_color(const ray *r0, const color* background, const hitable_list *world, unsigned int recursion_depth, unsigned int *seed)
{
    if(recursion_depth == 0)        // maximum recursion depth reached, return black
    {
        return (color) {0,0,0};
    }
    
    const ray* r = r0;
    
    //If the ray hits anything, take the color of the object
    hit_record rec;
    if (hitable_list_hit(world, r, 0.001, INFINITY, &rec, seed) == 0)
    {
        return *background;
    }
    
    //Check if the thing the ray hit scatter the ray somewhere else or absorbs the ray
    ray scattered;
    color attenuation;
    
    // check if the thing is a light source
    color emitted = material_emitted((material*)rec.mat, rec.u, rec.v, rec.p);
    
    // calculate scattered ray
    if(!material_scatter((material*)rec.mat, r, &rec, &attenuation, &scattered, seed))
    {
        return emitted;     // ray wasn't scatter, return only emitted light
    }
    
    // recursively follow the scattered ray and add it to the color
    color scattered_ray_color = ray_color(&scattered, background, world, recursion_depth-1, seed);
    
    // add up emitted and all scattered color and return
    return vec3_add(emitted, vec3_mult(attenuation, scattered_ray_color));
}

// create test scene with 1 huge sphere as ground and 5 smaller ones
hitable_list* test_scene()
{
    hitable_list* world= hitable_list_new("test scene");
    
    // create textures
    texture* ground_texture = texture_checker_init_byCol((color){0.2,0.3,0.1},(color){0.9,0.9,0.9});    // ground checker texture
    texture* blue_texture = texture_solid_color_init_rgb(0.1,0.2,0.5);        // blue texture
    texture* red_texture = texture_solid_color_init_rgb(0.7,0.2,0.3);         // red texture
    texture* glass_texture = texture_solid_color_init_rgb(1,0,0);       // red glass texture
    texture* gold_texture  = texture_solid_color_init_rgb(0.8,0.6,0.2);      // gold metal texture
    texture* metal_texture = texture_solid_color_init_rgb(0.1,1.0,0.1);     // green metal texture
    texture* light_tex = texture_solid_color_init_rgb(4,4,4);
    texture* light_tex2 = texture_solid_color_init_rgb(4.7,4.2,4.3);
    
    material* material_ground = material_lambertian_new(ground_texture);
    material* material_blue = material_lambertian_new(blue_texture);
    material* material_red = material_lambertian_new(red_texture);
    material* material_gold = material_metal_new(gold_texture, 0.0);
    material* material_glass = material_dielectric_new(glass_texture, 1.5);
    material* material_green_metal = material_metal_new(metal_texture, 0.0);
    material* material_light = material_light_new(light_tex);
    material* material_light2 = material_light_new(light_tex2);
    
    hitable_list_addObj(world, sphere_init((point3){0,-100.5,0}, 100, material_ground));      // ground sphere
    hitable_list_addObj(world, sphere_init((point3){-1,0,-1}, 0.5, material_blue));           // small blue sphere
    hitable_list_addObj(world, sphere_init((point3){0, 0, -2}, 0.3, material_red));      // small red sphere
    hitable_list_addObj(world, sphere_init((point3){0, 0, -1}, 0.5, material_glass));         // green glass sphere
    hitable_list_addObj(world, sphere_init((point3){0.7, 0.2, 0.3}, 0.3, material_green_metal));     // small metal sphere
    hitable_list_addObj(world, moving_sphere_init((point3){1,0.5,-1}, (point3) {1,0,-1}, 0, 1, 0.5, material_gold));
    hitable_list_addObj(world, sphere_init((point3){-1,0.1,-0.2}, 0.3, material_light2));    // floating light source
    hitable_list_addObj(world, sphere_init((point3){0, 0.5, -2}, 0.2, material_light));      // light inside red sphere
    
    return world;
}

hitable_list* random_scene()
{
    hitable_list* world = hitable_list_new("random scene");
    
//     //Add the ground object
//     material* material_ground = material_lambertian_new((vec3){0.5, 0.5, 0.5});
//     hitable* newObj = sphere_init((vec3) {0, -1000, 0}, 1000,  material_ground);
// 
//     //Add small random spheres
//     for (int a=-11; a<11; a++){
//         for (int b=-11; b<11; b++)
//         {
//             float choose_mat = random_float();
//             vec3 center = {a + 0.9*random_float(), 0.2, b + 0.9*random_float()};
//             vec3 p = {4, 0.2, 0};
//             if ( vec3_length(vec3_sub(center, p)) > 0.9){
//                 material* sphere_mat;
//                 if(choose_mat < 0.8){
//                     //diffuse
//                     float rand1 = random_float() * random_float();
//                     float rand2 = random_float() * random_float();
//                     float rand3 = random_float() * random_float();
//                     point3 center2 = vec3_add(center, (point3) {0, random_float_scaled(0,0.5), 0});
//                     color albedo = (color){rand1, rand2, rand3};
//                     sphere_mat = material_lambertian_new(albedo);
//                     hitable* moving_sphere = moving_sphere_init((vec3){center.e0, center.e1, center.e2}, center2, 0, 1.0, 0.2, sphere_mat);
//                     hitable_list_addObj(world, moving_sphere);
//                 }
//                 else if(choose_mat < 0.95)
//                 {
//                     //metal
//                     float rand1 = random_float_scaled(0.5,1);
//                     float rand2 = random_float_scaled(0.5,1);
//                     float rand3 = random_float_scaled(0.5,1);
//                     float fuzz = random_float_scaled(0, 0.5);
//                     sphere_mat = material_metal_new((vec3){rand1,rand2,rand3}, fuzz);
//                     hitable* metal_sphere = sphere_init((vec3){center.e0, center.e1, center.e2}, 0.2, sphere_mat);
//                     hitable_list_addObj(world, metal_sphere);
//                 }
//                 else
//                 {
//                     //glass
//                     sphere_mat = material_dielectric_new(1.5);
//                     hitable* glass_sphere = sphere_init((vec3){center.e0, center.e1, center.e2}, 0.2, sphere_mat);
//                     hitable_list_addObj(world, glass_sphere);
//                 }
//             }
//         }
//     }    
//     
//     //Add 3 big spheres with different materials and return
//     material* mat_glass = material_dielectric_new(1.5);
//     hitable* glass_sphere = sphere_init((vec3) {0, 1, 0}, 1.0, mat_glass);
//     
//     material* mat_brown = material_lambertian_new((vec3){0.4,0.2,0.1});
//     hitable* brown_sphere = sphere_init((vec3) {-4, 1, 0}, 1.0, mat_brown);
//   
//     material* mat_metal = material_metal_new((vec3){0.7, 0.6, 0.5}, 0.0);
//     hitable* metal_sphere = sphere_init((vec3){ 4, 1, 0}, 1.0, mat_metal);
//     
//     hitable_list_addObj(world, newObj);
//     hitable_list_addObj(world, glass_sphere);
//     hitable_list_addObj(world, brown_sphere);
//     hitable_list_addObj(world, metal_sphere);
    
    return world;
}

hitable_list* two_moving_spheres(texture_list* texlist, material_list* matlist)
{
    hitable_list* world = hitable_list_new("moving spheres");
    int brownTexIdx = texture_list_addTex(texlist, texture_solid_color_init_rgb(0.8,0.8,0.1));
    int brownMatIdx = material_list_addMat(matlist, material_lambertian_new(texlist->tex[brownTexIdx]));
    
    hitable_list_addObj(world, moving_sphere_init((point3){0,10,0}, (point3){0,0,0}, 10, 0, 1, matlist->mat[brownMatIdx]));
    
    return world;
}

hitable_list* two_spheres()
{
    hitable_list* objects = hitable_list_new("two spheres");
    
    texture* checker = texture_checker_init_byCol((color){0.1,0.2,0.1}, (color){0.9,0.9,0.9});
    material* spheremat = material_lambertian_new(checker);
    material* spheremat2 = material_lambertian_new(checker);
    
    hitable_list_addObj(objects, sphere_init((point3){0,-10,0}, 10, spheremat));
    hitable_list_addObj(objects, sphere_init((point3){0, 10,0}, 10, spheremat2));
    
    return objects;   
}

hitable_list* two_perlin_spheres()
{
    hitable_list* objects = hitable_list_new("two perlin spheres");
    texture_list* textures = texture_list_new("texture list");
    int texIdx = texture_list_addTex(textures, texture_noise_init((color){1,1,1},4));

    material_list* materials = material_list_new("material list");
    
    int marbleIdx = material_list_addMat(materials, material_lambertian_new(textures->tex[texIdx]));

    hitable_list_addObj(objects, sphere_init((point3){0,-1000,0}, 1000, materials->mat[marbleIdx]));
    hitable_list_addObj(objects, sphere_init((point3){0,2,0}, 2, materials->mat[marbleIdx]));
    
    return objects;    
}


hitable_list* earth()
{
    hitable_list* objects = hitable_list_new("image texture test");
    texture* earth_tex = texture_image_init("textures/earthmap.jpg");
    material* earth_mat = material_lambertian_new(earth_tex);
    hitable_list_addObj(objects, sphere_init((point3){0,0,0}, 2, earth_mat));
    
    return objects;
}

hitable_list *simple_light(texture_list* texlist, material_list* matlist)
{
    hitable_list* objects = hitable_list_new("simple rectangle light test");
    
//     texture_list* textures = texture_list_new("texture list");
//     material_list* materials = material_list_new("material list");
    
    int PerlIdx = texture_list_addTex(texlist, texture_noise_init((color){1,1,1}, 4));
    
    int matIdx = material_list_addMat(matlist, material_lambertian_new(texlist->tex[PerlIdx]));
    
    hitable_list_addObj(objects, sphere_init((point3){0,-1000,0},1000, matlist->mat[matIdx]));
    hitable_list_addObj(objects, sphere_init((point3){0,2,0}, 2, matlist->mat[matIdx]));
    
    int LightTexIdx = texture_list_addTex(texlist, texture_solid_color_init((color){4,4,4}));
    int matLightIdx = material_list_addMat(matlist, material_light_new(texlist->tex[LightTexIdx]));

    hitable_list_addObj(objects, xy_rect_init(3,5,1,3,-2, matlist->mat[matLightIdx]));
    hitable_list_addObj(objects, sphere_init((point3){0,7,0}, 2, matlist->mat[matLightIdx]));        
    
    return objects;    
}

hitable_list *cornell_box(texture_list* texlist, material_list* matlist)
{
    hitable_list* objects = hitable_list_new("Cornell Box");
    
    int red_idx = texture_list_addTex(texlist, texture_solid_color_init_rgb(0.65, 0.05, 0.05)); //) texture* red_tex = texture_solid_color_init_rgb(0.65, 0.05, 0.05);
    int white_idx = texture_list_addTex(texlist, texture_solid_color_init_rgb(0.73, 0.73, 0.73)); // texture* white_tex = texture_solid_color_init_rgb(0.73, 0.73, 0.73);
    int green_idx = texture_list_addTex(texlist, texture_solid_color_init_rgb(0.12, 0.45, 0.15)); //texture* green_tex = texture_solid_color_init_rgb(0.12, 0.45, 0.15);
    int light_idx = texture_list_addTex(texlist, texture_solid_color_init((color) {7,7,7}));
//     int glass_idx = texture_list_addTex(texlist, texture_solid_color_init_rgb(1,1,1));  //texture* glass_texture = texture_solid_color_init_rgb(1,1,1);
    int whSmoke_idx = texture_list_addTex(texlist, texture_solid_color_init_rgb(1,1,1));
    int blSmoke_idx = texture_list_addTex(texlist, texture_solid_color_init_rgb(0,0,0));
    
    int redMatIdx = material_list_addMat(matlist, material_lambertian_new(texlist->tex[red_idx]));
    int whiteMatIdx = material_list_addMat(matlist, material_lambertian_new(texlist->tex[white_idx]));
    int greenMatIdx = material_list_addMat(matlist, material_lambertian_new(texlist->tex[green_idx]));
    int lightMatIdx = material_list_addMat(matlist, material_light_new(texlist->tex[light_idx]));
    int whiteSmokeIdx = material_list_addMat(matlist, material_isotropic_new(texlist->tex[whSmoke_idx]));
    int darkSmokeIdx = material_list_addMat(matlist, material_isotropic_new(texlist->tex[blSmoke_idx]));
//     int glassMatIdx = material_list_addMat(matlist, material_dielectric_new(texlist->tex[glass_idx], 1.5));
    
    hitable_list_addObj(objects, yz_rect_init(0  , 555, 0  , 555, 555, matlist->mat[greenMatIdx]));
    hitable_list_addObj(objects, yz_rect_init(0  , 555, 0  , 555, 0  , matlist->mat[redMatIdx]));
    hitable_list_addObj(objects, xz_rect_init(113, 443, 127, 432, 554, matlist->mat[lightMatIdx]));
    hitable_list_addObj(objects, xz_rect_init(0  , 555, 0  , 555, 0  , matlist->mat[whiteMatIdx]));
    hitable_list_addObj(objects, xz_rect_init(0  , 555, 0  , 555, 555  , matlist->mat[whiteMatIdx]));
    hitable_list_addObj(objects, xy_rect_init(0  , 555, 0  , 555, 555  , matlist->mat[whiteMatIdx]));
    
    hitable* box1 = box_init((point3){0,0,0}, (point3){165,330,165}, matlist->mat[whiteMatIdx]);
    box1 = instance_rotate_init(box1, 15, Y);
    box1 = instance_translate_init(box1, (vec3){265, 0, 295});
    box1 = constant_medium_init(box1, matlist->mat[darkSmokeIdx], 0.01);
        
    hitable* box2 = box_init((point3){0,0,0}, (point3){165,165,165}, matlist->mat[whiteMatIdx]);
    box2 = instance_rotate_init(box2, -18, Y);
    box2 = instance_translate_init(box2, (vec3){130,0,65});
    box2 = constant_medium_init(box2, matlist->mat[whiteSmokeIdx], 0.01);
    
    hitable_list_addObj(objects, box1);
    hitable_list_addObj(objects, box2);
//     hitable_list_addObj(objects, box_init((point3){130,0,65},(point3){295,165,230}, white));
//     hitable_list_addObj(objects, box_init((point3){265,0,295},(point3){430,330,460}, white));
    
//     hitable_list_addObj(objects, sphere_init((point3){212.5,215,147.5}, 50, matlist->mat[glassMatIdx]));
//     hitable_list_addObj(objects, sphere_init((point3){450, 50, 120}, 50, matlist->mat[lightMatIdx]));
    return objects;
}

hitable_list *final_scene(texture_list* texlist, material_list* matlist)
{
    hitable_list *floor = hitable_list_new("floor full of boxes");
    int FloorTexIdx = texture_list_addTex(texlist, texture_solid_color_init_rgb(0.48,0.83,0.53));
    int FloorMatIdx = material_list_addMat(matlist, material_lambertian_new(texlist->tex[FloorTexIdx]));
    
    const int boxes_per_side = 20;
    for (int i = 0; i < boxes_per_side; i++) 
    {
        for (int j = 0; j < boxes_per_side; j++) 
        {
            float w = 100.0;
            float x0 = -1000.0 + i*w;
            float z0 = -1000.0 + j*w;
            float y0 = 0.0;
            float x1 = x0 + w;
            float y1 = random_float_scaled(1,101);
            float z1 = z0 + w;
            hitable_list_addObj(floor, box_init((point3){x0,y0,z0}, (point3){x1,y1,z1}, matlist->mat[FloorMatIdx]));
        }
    }    
    
    // wrap  box floor into BVH tree
    hitable_list* objects = hitable_list_new("BVH floor");
    hitable* bvh_floor = bvh_node_init(floor, 0, 1);
    hitable_list_addObj(objects, bvh_floor);
    
    // light source
    int LightTexIdx = texture_list_addTex(texlist, texture_solid_color_init_rgb(7,7,7));
    int LightMatIdx = material_list_addMat(matlist, material_light_new(texlist->tex[LightTexIdx])); 
    hitable_list_addObj(objects, xz_rect_init(123,423,147,412,554, matlist->mat[LightMatIdx]));
    
    // moving sphere
    point3 center1 = (point3){400, 400, 200};
    point3 center2 = vec3_add(center1, (point3){30,0,0});
    int MovSpTexIdx = texture_list_addTex(texlist, texture_solid_color_init_rgb(0.7,0.3,0.1));
    int MovSpMatIdx = material_list_addMat(matlist, material_lambertian_new(texlist->tex[MovSpTexIdx]));
    hitable_list_addObj(objects, moving_sphere_init(center1, center2, 50, 0, 1, matlist->mat[MovSpMatIdx]));
    
    // glass sphere
    int GlassTexIdx = texture_list_addTex(texlist, texture_solid_color_init_rgb(1,1,1));
    int GlassMatIdx = material_list_addMat(matlist, material_dielectric_new(texlist->tex[GlassTexIdx], 1.5));
    hitable_list_addObj(objects, sphere_init((point3){260,150,45}, 50, matlist->mat[GlassMatIdx]));
    
    // metal sphere
    int MetalTexIdx = texture_list_addTex(texlist, texture_solid_color_init_rgb(0.8,0.8,0.9));
    int MetalMatIdx = material_list_addMat(matlist, material_metal_new(texlist->tex[MetalTexIdx], 1.0));
    hitable_list_addObj(objects, sphere_init((point3){0,150,145}, 50, matlist->mat[MetalMatIdx]));
    
    // blue sphere with subsurface scattering
    hitable* boundary = sphere_init((point3){360,150,145}, 70, matlist->mat[GlassMatIdx]);
    hitable_list_addObj(objects, boundary);
    int SubTexIdx = texture_list_addTex(texlist, texture_solid_color_init_rgb(0.2,0.4,0.9));
    int SubMatIdx = material_list_addMat(matlist, material_isotropic_new(texlist->tex[SubTexIdx]));
    hitable_list_addObj(objects, constant_medium_init(boundary, matlist->mat[SubMatIdx], 0.2));
    
    // Volumetric fog for whole scene
    int FogMatIdx = material_list_addMat(matlist, material_isotropic_new(texlist->tex[GlassMatIdx]));
    boundary = sphere_init((point3){0,0,0}, 5000, matlist->mat[FogMatIdx]);
    hitable* fog = constant_medium_init(boundary, matlist->mat[FogMatIdx], 0.0001);
    hitable_list_addObj(objects, fog);
    
    // earth sphere
    int EarthTexIdx = texture_list_addTex(texlist, texture_image_init("textures/earthmap.jpg"));
    int EarthMatIdx = material_list_addMat(matlist, material_lambertian_new(texlist->tex[EarthTexIdx]));
    hitable_list_addObj(objects, sphere_init((point3){400, 200, 400}, 100, matlist->mat[EarthMatIdx]));
        
    // perlin noise sphere
    int PerTexIdx = texture_list_addTex(texlist, texture_noise_init((color){1,1,1}, 5));
    int PerMatIdx = material_list_addMat(matlist, material_lambertian_new(texlist->tex[PerTexIdx]));
    hitable_list_addObj(objects, sphere_init((point3){220,280,300}, 80, matlist->mat[PerMatIdx]));
    
    // 1000 small spheres grouped, translated and rotated
    int WhiteTexIdx = texture_list_addTex(texlist, texture_solid_color_init_rgb(0.73,0.73,0.73));
    int WhiteMatIdx = material_list_addMat(matlist, material_lambertian_new(texlist->tex[WhiteTexIdx]));
    
    unsigned int num_sph = 1000;
    hitable_list* many_spheres = hitable_list_new("1000 small spheres");
    for(unsigned int j = 0; j < num_sph; j++)
    {
        hitable_list_addObj(many_spheres, sphere_init(vec3_random_scaled(0,165), 10, matlist->mat[WhiteMatIdx]));
    }
    
    hitable* bvh_smallsp = bvh_node_init(many_spheres, 0, 1);
    hitable* rotated_spheres = instance_rotate_init(bvh_smallsp, 15, Y);
    hitable* translated_spheres = instance_translate_init(rotated_spheres, (vec3){-100,270,395});
    hitable_list_addObj(objects, translated_spheres);
    
    return objects;
}

int main(int argc, char** argv)
{
    if(argc < 2)
    {
        printf("\tUsage: yapt output (default is png, you can specify format by extension: {.bmp .tga .png .jpg} \n");
        return(1);
    }
    
    // Output File
     char* outputFile = argv[1];
    
    // define image and quality
    float ASPECT_RATIO = 1.0;   //16.0 / 9.0;
    int IMAGE_WIDTH = 300;
    int IMAGE_HEIGHT = (int) (IMAGE_WIDTH / ASPECT_RATIO);
    
    int samples_per_pixel = 100;   // nr of samples per pixel
    const int max_recursion_depth = 30;     // max of scattered rays
    
//  setup image buffer
    const int CHANNELS = 3;    // rgb
    unsigned char pixels[IMAGE_WIDTH * (IMAGE_HEIGHT+1) * CHANNELS];       // array that holds r,g,b for first pixel, then r,g,b for 2nd and so on
    
    hitable_list* world;
    texture_list* textures = texture_list_new("world textures");
    material_list* materials = material_list_new("world materials");
    
    point3 lookfrom;
    point3 lookat;
    float vfov = 40;
    float aperture = 0.0;
    float dist_to_focus = 10;
    color background = (color) {1,1,1};
    
    switch(9)
    {
        case 1:
            world = random_scene();
            background = (color){0.7,0.8,1.0};
            lookfrom = (point3){13,2,3};
            lookat = (point3){0,0,0};
            vfov = 20;
            aperture = 0.1;
            break;
        case 2:
            world = two_moving_spheres(textures, materials);
            background = (color){0.7,0.8,1.0};
            lookfrom = (point3){13,2,3};
            lookat = (point3){0,0,0};
            vfov = 20;
            break;
        case 3:
            world = two_spheres();
            background = (color){0.7,0.8,1.0};
            lookfrom = (point3){13,2,3};
            lookat = (point3){0,0,0};
            vfov = 20;
            break;
        case 4: 
            world = two_perlin_spheres();
            background = (color){0.7,0.8,1.0};
            lookfrom = (point3){13,2,3};
            lookat = (point3){0,0,0};
            vfov = 20;           
            break;
        case 5:
            world = earth();
            background = (color){0.7,0.8,1.0};
            lookfrom = (point3){13,2,3};
            lookat = (point3){0,0,0};
            vfov = 20;              
            break;
        case 6:
            world = simple_light(textures, materials);
            samples_per_pixel = 400; 
            background = (color){0.0,0.0,0.0};
            lookfrom = (point3){26,3,6};
            lookat = (point3){0,2,0};
            vfov = 20;              
            break;
        case 7:
            world = cornell_box(textures, materials);
            ASPECT_RATIO = 1.0;
//             IMAGE_WIDTH = 600;
//             samples_per_pixel = 400;
            background = (color){0.0,0.0,0.0};
            lookfrom = (point3){278,278, -800};
            lookat = (point3){278,278,0};
            vfov = 40;   
            break;
        case 8:
            world = test_scene();
            background = (color){0.01,0.01,0.01};
            lookfrom = (point3){4,2,5};
            lookat = (point3){0,0,-1};
            vfov = 20;
//             aperture = 0.1;
            break;
        case 9:
            world = final_scene(textures, materials);
//             IMAGE_WIDTH = 800;
//             samples_per_pixel = 100;
            ASPECT_RATIO = 1.0;
            background = (color){0.0,0.0,0.0};
            lookfrom = (point3){478,278, -600};
            lookat = (point3){278,278,0};
            vfov = 40;   
            break;            
        default:
            background = (color){0,0,0};
    }
    
    printf("\nScene \"%s\" with %i objects\n", world->listname, world->list_size);
    camera cam;
    
    camera_init(&cam, lookfrom, lookat, (vec3){0,1,0}, vfov, ASPECT_RATIO, aperture, dist_to_focus, 0, 1);

    
    // setup time counter
    struct timespec starttime, finishtime;
    double elapsed;
    clock_gettime(CLOCK_MONOTONIC, &starttime);
    
    unsigned int seed = time(NULL);
    
    const unsigned int tilesize_i = 80;
    const unsigned int tilesize_j = 80;
    const unsigned int nr_tiles_j = IMAGE_HEIGHT / tilesize_j;
    
    const unsigned int nr_tiles = (unsigned int) (IMAGE_WIDTH / tilesize_i) * (IMAGE_HEIGHT / tilesize_j);
   
    unsigned int tiles_done = 0;
    
    // create an array with all tile indices
    unsigned int tilepool[nr_tiles];
    for(unsigned i = 0; i<nr_tiles; i++)
    {
        tilepool[i] = i;
    }
    
    // shuffle tile pool to ensure all threads will render different parts
    // of the scene (because difficulty might be unevenly distributed)
    for(unsigned int j = nr_tiles-1; j > 0; j--)
    {
        unsigned int swap_idx = rand() % (j+1);
        unsigned int tmp = tilepool[j];
        tilepool[j] = tilepool[swap_idx];
        tilepool[swap_idx] = tmp;
    }
    
    // Parallelization starts here, threads handle tiles in parallel
    const unsigned int THREADS = 4;
    #pragma omp parallel for num_threads(THREADS) shared(tiles_done)
    for(unsigned int tile_idx = 0; tile_idx< nr_tiles; tile_idx++)
    {
        // create a separate seed for each thread´s own random number generator
        unsigned int thread_id = omp_get_thread_num();
        unsigned int thread_seed = seed + thread_id;
        unsigned int tile_i, tile_j;
        
        // calculate tile coordinates (0..nr_tiles_XY)
        tile_i = tilepool[tile_idx] % nr_tiles_j;
        tile_j = (unsigned int) (tilepool[tile_idx] / nr_tiles_j);
        
        ///////////////////////////////////////////////////////////////////////
        // RENDER LOOP (render in screen coordinates, translated from tile   //
        // coordinates)                                                      //
        ///////////////////////////////////////////////////////////////////////
        // loop in y direction
        for(unsigned int sj = (tile_j*tilesize_j); sj <  ((tile_j + 1) * tilesize_j); sj++)
        {
            // loop in x direction
            for(unsigned int si = (tile_i*tilesize_i); si < ((tile_i+1)*tilesize_i); si++)
            {
                color pixel_color = {0,0,0};
                // brute force anti-aliasing loop
                for(int s=0; s < samples_per_pixel; ++s)   
                {
                    float random1 = (float)rand_r(&thread_seed) / (float)((unsigned)RAND_MAX + 1);    // create 2 independent
                    float random2 = (float)rand_r(&thread_seed) / (float)((unsigned)RAND_MAX + 1);    // random numbers 0 <= x < 1
                    
                    float u = (float) (si+random1) / (float) IMAGE_WIDTH;     // "noisify" pixel in x
                    float v = (float) (sj+random2) / (float) IMAGE_HEIGHT;     // and y coordinate
                    ray r = camera_get_ray(&cam, u,v, &thread_seed);      // shoot a ray for each noisyfied pixel
                    pixel_color = vec3_add( pixel_color, ray_color(&r, &background, world, max_recursion_depth, &thread_seed));      // add up all noisyfied pixel colors
                }
        
                // add current pixel to pixel array/image
                unsigned int pix_index = (si + (sj*IMAGE_WIDTH)) * CHANNELS;
                add_pixel_to_imagebuffer (pixels, &pixel_color, samples_per_pixel, pix_index);                
            }
        }
        tiles_done++;
        printf("\nThread %i finished tile %i (%i/%i done) %.0f%%", thread_id, tilepool[tile_idx], tiles_done, nr_tiles, (float)tiles_done/nr_tiles*100 ); 
    }     

    save_image(outputFile, IMAGE_WIDTH, IMAGE_HEIGHT, CHANNELS, pixels);
    
    clock_gettime(CLOCK_MONOTONIC, &finishtime);
    elapsed = (finishtime.tv_sec - starttime.tv_sec);
    elapsed += (finishtime.tv_nsec - starttime.tv_nsec) / 1000000000.0;
    //Output statistics
    unsigned long raycount = IMAGE_HEIGHT * IMAGE_WIDTH * samples_per_pixel;
    printf("\n\nTime elapsed for rendering: %.2f s\n", elapsed);
    printf("Nr. of primary Rays: %'lu\n", raycount);
    double Mrayspersecond = (raycount/1000000) / elapsed; 
    printf("%f Mrays/s\n" , Mrayspersecond);
    
    // cleanup
    material_list_free(materials);
    texture_list_free(textures);
    hitable_list_free(world);
    
      
    return EXIT_SUCCESS;
}
