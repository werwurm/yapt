#ifndef AARECT_H
#define AARECT_H

#include "material.h"
#include "hitable.h"

// class definitions

//  y
//  ^   z
//  |  ^ 
//  | /
//  |/
//  +----> x

// XY-aligned rectangles
typedef struct
{
    float x0,x1,y0,y1,k;
    
    struct material* mat;
} xy_rect;

// XZ-aligned rectangles
typedef struct
{
    float x0,x1,z0,z1,k;
    
    struct material* mat;
} xz_rect;

// YZ-aligned rectangles
typedef struct
{
    float y0,y1,z0,z1,k;
    
    struct material* mat;
} yz_rect;

// constructors
hitable* xy_rect_init(float x0, float x1, float y0, float y1, float k, material* mat);
hitable* xz_rect_init(float x0, float x1, float z0, float z1, float k, material* mat);
hitable* yz_rect_init(float y0, float y1, float z0, float z1, float k, material* mat);

// destructors
void xy_rect_free(hitable* object);
void xz_rect_free(hitable* object);
void yz_rect_free(hitable* object);

// bounding boxes
bool xy_rect_create_aabb(xy_rect* rect, aabb* output_box);
bool xz_rect_create_aabb(xz_rect* rect, aabb* output_box);
bool yz_rect_create_aabb(yz_rect* rect, aabb* output_box);

// ray intersection code
bool xy_rect_hit(const xy_rect* rect, const ray *r, float t_min, float t_max, hit_record *rec);
bool xz_rect_hit(const xz_rect* rect, const ray *r, float t_min, float t_max, hit_record *rec);
bool yz_rect_hit(const yz_rect* rect, const ray *r, float t_min, float t_max, hit_record *rec);

#endif // AARECT_H
