#include<stdio.h>
#include<stdbool.h>

#include "aarect.h"
#include "hitable.h"
#include "vec3.h"

//#############################################################################
// constructors
//#############################################################################
// constructor for XY-aligned rectangle objects
hitable* xy_rect_init(float x0, float x1, float y0, float y1, float k, material* mat)
{
    // create rectangle
    xy_rect* rect = (xy_rect*) malloc(sizeof(xy_rect));    
    
    rect->x0 = x0;
    rect->x1 = x1;
    rect->y0 = y0;
    rect->y1 = y1;
    rect->k = k;
    
    rect->mat = (struct material*) mat;    
    
    // create generic object
    hitable* object = hitable_generic_init(HITABLE_RECT_XY, rect);
    return object;
}

// constructor for XZ-aligned rectangle objects
hitable* xz_rect_init(float x0, float x1, float z0, float z1, float k, material* mat)
{
    // create rectangle
    xz_rect* rect = (xz_rect*) malloc(sizeof(xz_rect));    
    
    rect->x0 = x0;
    rect->x1 = x1;
    rect->z0 = z0;
    rect->z1 = z1;
    rect->k = k;
    
    rect->mat = (struct material*) mat;    
    
    // create generic object
    hitable* object = hitable_generic_init(HITABLE_RECT_XZ, rect);
    return object;    
}

// constructor for YZ-aligned rectangle objects
hitable* yz_rect_init(float y0, float y1, float z0, float z1, float k, material* mat)
{
    // create rectangle
    yz_rect* rect = (yz_rect*) malloc(sizeof(yz_rect));    
    
    rect->y0 = y0;
    rect->y1 = y1;
    rect->z0 = z0;
    rect->z1 = z1;
    rect->k = k;
    
    rect->mat = (struct material*) mat;    
    
    // create generic object
    hitable* object = hitable_generic_init(HITABLE_RECT_YZ, rect);
    return object;    
}

//#############################################################################
// destructors
//#############################################################################
void xy_rect_free(hitable* object)
{
    if(object->type == HITABLE_RECT_XY)
    {
        // upcast object to rect type
        xy_rect* temprect = object->obj;

        free(temprect);
        free(object);
    }
    else
    {
        printf("\ncalling xy_rect_free() for object of type %d. Aborting now\n", object->type);
        exit(EXIT_FAILURE);
    }    
}

void yz_rect_free(hitable* object)
{
    if(object->type == HITABLE_RECT_YZ)
    {
        // upcast object to rect type
        yz_rect* temprect = object->obj;

        free(temprect);
        free(object);
    }
    else
    {
        printf("\ncalling yz_rect_free() for object of type %d. Aborting now\n", object->type);
        exit(EXIT_FAILURE);
    }    
}

void xz_rect_free(hitable* object)
{
    if(object->type == HITABLE_RECT_XZ)
    {
        // upcast object to rect type
        xz_rect* temprect = object->obj;

        free(temprect);
        free(object);
    }
    else
    {
        printf("\ncalling xz_rect_free() for object of type %d. Aborting now\n", object->type);
        exit(EXIT_FAILURE);
    }    
}

//#############################################################################
// Bounding Boxes
//#############################################################################
bool xy_rect_create_aabb(xy_rect* rect, aabb* output_box)
{
    output_box = aabb_init((point3){rect->x0, rect->y0, rect->k-0.0001}, (point3){rect->x1, rect->y1, rect->k+0.0001});
    return true;
}

bool yz_rect_create_aabb(yz_rect* rect, aabb* output_box)
{
    output_box = aabb_init((point3){rect->k-0.0001, rect->y0, rect->z0}, (point3){rect->k+0.0001, rect->y1, rect->z1});
    return true;
}

bool xz_rect_create_aabb(xz_rect* rect, aabb* output_box)
{
    output_box = aabb_init((point3){rect->x0, rect->k-0.0001, rect->z0}, (point3){rect->x1, rect->k+0.0001, rect->z1});
    return true;
}

//#############################################################################
// Ray intersection code
//#############################################################################
bool xy_rect_hit(const xy_rect* rect, const ray *r, float t_min, float t_max, hit_record *rec)
{
    float t = (rect->k - r->origin.e2) / r->direction.e2;
    if((t< t_min) || (t>t_max))
    {
        return false;
    }
    
    float x = r->origin.e0 + t*r->direction.e0;
    float y = r->origin.e1 + t*r->direction.e1;
    if((x < rect->x0) || (x > rect->x1) || (y < rect->y0) || (y > rect->y1))
    {   
        return false;
    }
    
    rec->u = (x - rect->x0) / (rect->x1 - rect->x0);
    rec->v = (y - rect->y0) / (rect->y1 - rect->y0);
    rec->t = t;
    
    vec3 outward_normal = (vec3){0,0,1};  
    hit_record_set_facenormal(rec, r, &outward_normal);

    rec->mat = rect->mat;
    rec->p = ray_point_at_parameter(*r, t);
    
    return true;
}

bool xz_rect_hit(const xz_rect* rect, const ray *r, float t_min, float t_max, hit_record *rec)
{
    float t = (rect->k - r->origin.e1) / r->direction.e1;
    if((t< t_min) || (t>t_max))
    {
        return false;
    }
    
    float x = r->origin.e0 + t*r->direction.e0;
    float z = r->origin.e2 + t*r->direction.e2;
    if((x < rect->x0) || (x > rect->x1) || (z < rect->z0) || (z > rect->z1))
    {   
        return false;
    }
    
    rec->u = (x - rect->x0) / (rect->x1 - rect->x0);
    rec->v = (z - rect->z0) / (rect->z1 - rect->z0);
    rec->t = t;
    
    vec3 outward_normal = (vec3){0,1,0};  
    hit_record_set_facenormal(rec, r, &outward_normal);

    rec->mat = rect->mat;
    rec->p = ray_point_at_parameter(*r, t);
    
    return true;    
}

bool yz_rect_hit(const yz_rect* rect, const ray *r, float t_min, float t_max, hit_record *rec)
{
    float t = (rect->k - r->origin.e0) / r->direction.e0;
    if((t< t_min) || (t>t_max))
    {
        return false;
    }
    
    float y = r->origin.e1 + t*r->direction.e1;
    float z = r->origin.e2 + t*r->direction.e2;
    if((y < rect->y0) || (y > rect->y1) || (z < rect->z0) || (z > rect->z1))
    {   
        return false;
    }
    
    rec->u = (y - rect->y0) / (rect->y1 - rect->y0);
    rec->v = (z - rect->z0) / (rect->z1 - rect->z0);
    rec->t = t;
    
    vec3 outward_normal = (vec3){1,0,0};  
    hit_record_set_facenormal(rec, r, &outward_normal);

    rec->mat = rect->mat;
    rec->p = ray_point_at_parameter(*r, t);
    
    return true;     
}
