#ifndef AABB_H
#define AABB_H

#include<stdbool.h>

#include "vec3.h"
#include "ray.h"

// Axis-Aligned Bounding Box
typedef struct
{
    point3 minimum;
    point3 maximum;
} aabb;

// constructor
aabb* aabb_init(point3 minimum, point3 maximum);

// destructor
void aabb_free(aabb* the_box);

// ray intersection code
bool aabb_hit(const aabb* box, const ray* r, float t_min, float t_max);

// create bigger surrounding box for 2 smaller aabb
aabb create_surrounding_box(aabb box0, aabb box1);

// swaps value between two floats
void swapValue(float *val1, float *val2);

#endif  // AABB_H
