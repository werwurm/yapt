#ifndef RAY_H
#define RAY_H

#include "vec3.h"

typedef struct 
{
    point3 origin;
    point3 direction;
    float time;
} ray;


vec3 ray_origin ( const ray self );
vec3 ray_direction ( const ray self );
vec3 ray_point_at_parameter ( const ray self, float t );

#endif
