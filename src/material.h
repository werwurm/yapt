#ifndef MATERIAL_H
#define MATERIAL_H

#include<stdbool.h>

#include "vec3.h"
#include "ray.h"
#include "hitable.h"
#include "texture.h"


typedef enum
{
    MATERIAL_LAMBERTIAN,
    MATERIAL_METAL,
    MATERIAL_DIELECTRIC,
    MATERIAL_PHONG,
    MATERIAL_LIGHT,
    MATERIAL_ISOTROPIC
} material_type;

// generic material struct
typedef struct 
{
    material_type type;
    void* mat;
} material;

// data structure that will hold all textures 
typedef struct
{
    char* listname;
    material** mat;
    unsigned int list_size;    
} material_list;

//Materials
typedef struct{texture* albedo;} material_lambertian;
typedef struct{texture* albedo; float fuzz;} material_metal;
typedef struct{texture* albedo; float ir;} material_dielectric;
typedef struct{texture* emit;} material_light;
typedef struct{texture* albedo;} material_isotropic;

// "constructors"
material* material_lambertian_new(texture* albedo);
material* material_metal_new(texture* albedo, float fuzz);
material* material_dielectric_new(texture* albedo, float ir);
material* material_light_new(texture* emit);
material* material_light_new_color(color col);
material* material_isotropic_new(texture* albedo);
material_list* material_list_new(char* listname);

int material_list_addMat(material_list* the_list, material* const newMat);

// "destructors"
void material_free(material* mat);
void material_list_free(material_list* the_list);

vec3 reflect(const vec3 *v, const vec3 *n);
bool refract(const vec3 *v, const vec3 *n, float ni_over_nt, vec3 *refracted);
float schlick(float cosine, float ref_idx);



bool material_scatter(material* mat, const ray* r, hit_record* rec, color* attenuation, ray* scattered, unsigned int *seed);
color material_emitted(material* mat, float u, float v, point3 p);

vec3 random_in_unit_sphere();

#endif
