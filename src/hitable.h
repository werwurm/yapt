#ifndef HITABLE_H
#define HITABLE_H

#include <stdbool.h>

#include "AABB.h"
#include "vec3.h"
#include "ray.h"

// stores data when rays hit an object
typedef struct
{
    vec3 p;
    vec3 normal;
    struct material* mat; 
    float t;
    float u;    // u texture coordinate where ray hits
    float v;    // v texture coordinate where ray hits
    bool front_face;    // normal always points outside surface, if ray and normal face the same direction, we are inside the object
} hit_record; 

typedef enum
{
    HITABLE_SPHERE, 
    HITABLE_MOVING_SPHERE, 
    HITABLE_BVH_NODE,
    HITABLE_RECT_XY,
    HITABLE_RECT_XZ,
    HITABLE_RECT_YZ,
    HITABLE_BOX,
    HITABLE_TRANSLATE,
    HITABLE_ROTATE,
    HITABLE_CONSTANT_MEDIUM
} hitable_type;

// generic hitable object
typedef struct
{
    hitable_type type;
    void* obj;
} hitable;


///Set the front_face and normal property of the hit record object based on the ray and normal
static inline void hit_record_set_facenormal(hit_record* rec, const ray* r, const vec3* outward_normal)
{
    rec->front_face = vec3_dot(r->direction, *outward_normal) < 0;
    rec->normal = rec->front_face ? vec3_copy(outward_normal) : vec3_flip(outward_normal);
}

// generic "constructor" for all hitable objects
hitable* hitable_generic_init(hitable_type type, void* object);

// generic "destructor" for all hitable objects
void hitable_generic_free(hitable* object);

// generic function to create bounding box for all supported objects
int hitable_create_aabb(hitable* the_object, float t0, float t1, aabb* bounding_box);

// generic ray intersection function
bool hitable_object_hit(const hitable *obj, const ray *r, const float t_min, const float t_max, hit_record *rec, unsigned int *seed);

#endif

