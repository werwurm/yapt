#include<math.h>

#include "camera.h"
#include "utils.h"
#include "vec3.h"

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif


void camera_init(camera *the_cam, vec3 lookfrom, vec3 lookat, vec3 vecup, float vfov, float aspect, float aperture, float focus_dist, float time0, float time1)
{
    the_cam->lens_radius = aperture / 2;

    float theta = vfov * M_PI / 180.0f;
    float half_height = tan(theta / 2.0f);
    float half_width = aspect * half_height;

    the_cam->origin = lookfrom;
    the_cam->w = vec3_unit_vector(vec3_sub(lookfrom , lookat));
    the_cam->u = vec3_unit_vector(vec3_cross(vecup,the_cam->w));
    the_cam->v = vec3_cross(the_cam->w,the_cam->u);

    the_cam->lower_left_corner = vec3_sub(the_cam->origin, vec3_skalarMult(the_cam->u, half_width * focus_dist));
    the_cam->lower_left_corner = vec3_sub(the_cam->lower_left_corner, vec3_skalarMult(the_cam->v, half_height*focus_dist));
    the_cam->lower_left_corner = vec3_sub(the_cam->lower_left_corner, vec3_skalarMult(the_cam->w,focus_dist));
    the_cam->horizontal = vec3_skalarMult(the_cam->u, 2*half_width*focus_dist);
    the_cam->vertical = vec3_skalarMult(the_cam->v, 2*half_height*focus_dist);
    
    the_cam->time0 = time0;
    the_cam->time1 = time1;
}

// generate a ray from camera to scene
ray camera_get_ray(camera *the_cam, float s, float t, unsigned int *seed)
{
    vec3 rd = vec3_skalarMult(random_in_unit_disc_multithread(seed), the_cam->lens_radius);
    vec3 offset = vec3_skalarMult(the_cam->u, rd.e0);
    offset = vec3_add(offset, vec3_skalarMult(the_cam->v, rd.e1));
    
    vec3 tmpvec1 = vec3_add(the_cam->lower_left_corner, vec3_skalarMult(the_cam->horizontal, s));
    tmpvec1 = vec3_add(tmpvec1, vec3_skalarMult(the_cam->vertical, t));
    tmpvec1 = vec3_sub(tmpvec1, the_cam->origin);
    tmpvec1 = vec3_sub(tmpvec1, offset);
    
    ray r= {.origin = vec3_add(the_cam->origin, offset), 
            .direction = tmpvec1, 
            .time = random_float_scaled_multithread(the_cam->time0, the_cam->time1, seed)};
    
    return r;
}
