// Perlin noise data and functions to be used by texture.c
#ifndef PERLIN_H
#define PERLIN_H

#include "vec3.h"


#define POINT_COUNT 256


typedef struct
{
//     float* ranfloat;
    vec3* ranvec;
    unsigned int* perm_x;
    unsigned int* perm_y;
    unsigned int* perm_z;
} perlin;

// Constructor
perlin* perlin_new();

// destructor
void perlin_free(perlin* p);


float perlin_noise(perlin* p, point3 point);
float perlin_turbulence(perlin* perl, const point3 p, unsigned int depth);

#endif // PERLIN_H
