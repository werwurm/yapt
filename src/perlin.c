// Perlin noise functions, utilized by Textures
#include<math.h>

#include "perlin.h"
#include "utils.h"


// Trilinear interpolation 
static float trilinear_interp(vec3 c[2][2][2], float u, float v, float w)
{
    // Hermetian smoothing to prevent grid artifacts in texture
    float uu = u*u*(3-2*u);
    float vv = v*v*(3-2*v);
    float ww = w*w*(3-2*w);
    
    float accum = 0.0;
    
    for(unsigned int i=0; i<2; i++)
    {
        for(unsigned int j=0; j< 2; j++)
        {
            for(unsigned int k=0; k<2; k++)
            {
                vec3 weight_v= (vec3){u-i, v-j, w-k};
                    
                accum = accum + (i*uu + (1-i)*(1-uu)) *
                                (j*vv + (1-j)*(1-vv)) *
                                (k*ww + (1-k)*(1-ww)) *
                                vec3_dot(c[i][j][k], weight_v);
            }
        }
    }
    return accum;
}

// randomly switch values within int array p
static void perlin_permute(unsigned int* p, int pcount)
{
    for(unsigned int i = pcount-1; i>0; i--)
    {
        unsigned int target = (unsigned int) random_float_scaled(0,i);
        unsigned int tmp = p[i];
        p[i] = p[target];
        p[target] = tmp;
    }
}

static unsigned int* perlin_generate_perm()
{
    unsigned int* p = (unsigned int*) malloc(sizeof(unsigned int) * POINT_COUNT);
    
    for(unsigned int i=0; i<POINT_COUNT; i++)
    {
        p[i] = i;
    }
        perlin_permute(p, POINT_COUNT);
        return p;
}

// Constructor
perlin* perlin_new()
{
    perlin* new_p = (perlin*) malloc(sizeof(perlin));
    
    new_p->ranvec = (vec3*) malloc(sizeof(vec3)*POINT_COUNT);
    
    for(unsigned int i=0; i<POINT_COUNT; i++)
    {
        new_p->ranvec[i] = vec3_unit_vector((vec3){random_float_scaled(-1,1), 
                                            random_float_scaled(-1,1),
                                            random_float_scaled(-1,1)});
    }
    
    new_p->perm_x = perlin_generate_perm();
    new_p->perm_y = perlin_generate_perm();
    new_p->perm_z = perlin_generate_perm();
    
    return new_p;    
}

// destructor
void perlin_free(perlin* p)
{
    free(p->ranvec);
    free(p->perm_x);
    free(p->perm_y);
    free(p->perm_z);
    free(p);
}


float perlin_noise(perlin* p, point3 point)
{
    float u = point.e0 - floor(point.e0);
    float v = point.e1 - floor(point.e1);
    float w = point.e2 - floor(point.e2);
       
    unsigned int i = (unsigned int) floor(point.e0);
    unsigned int j = (unsigned int) floor(point.e1);
    unsigned int k = (unsigned int) floor(point.e2);

    vec3 c[2][2][2];
    
    for(unsigned int di=0; di<2; di++)
    {
        for(unsigned int dj=0; dj<2; dj++)
        {
            for(unsigned int dk=0; dk<2; dk++)
            {
                c[di][dj][dk] = p->ranvec[p->perm_x[(i+di) & 255]^
                                            p->perm_y[(j+dj) & 255]^
                                            p->perm_z[(k+dk) & 255]];
            }
        }        
    }
    
    return trilinear_interp(c,u,v,w);
}

// calculate composite noise with multiple perlin noise frequencies
float perlin_turbulence(perlin* perl, const point3 p, unsigned int depth)
{
    float accum = 0.0;
    point3 temp_p = p;
    float weight = 1.0;
    
    for(unsigned int i = 0; i<depth; i++)
    {
        accum = accum + weight*perlin_noise(perl, temp_p);
        weight = weight * 0.5;
        temp_p = vec3_skalarMult(temp_p, 2);
    }
    
    return fabsf(accum);
}
