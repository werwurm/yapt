#ifndef VEC3_H
#define VEC3_H

#include <math.h>
#include <stdlib.h>

// y,e1
//  ^   z,e2
//  |  ^ 
//  | /
//  |/
//  +----> x,e0

typedef struct
{
    float e0,e1,e2;
} vec3;

typedef vec3 color;     // colors also have 3 components 
typedef vec3 point3;    // as do points in 3d space

extern vec3 vec3_skalarMult(const vec3 self, const float t);
extern vec3 vec3_skalarDiv(const vec3 self, const float t);
extern vec3 vec3_add(const vec3 vector1, const vec3 vector2);
extern vec3 vec3_sub(const vec3 vector1, const vec3 vector2);
extern vec3 vec3_mult(const vec3 vec1, const vec3 vec2);
extern vec3 vec3_div(const vec3 vect1, const vec3 vect2);
extern vec3 vec3_cross(const vec3 vec1, const vec3 vect2);
extern float vec3_dot(const vec3 vect1, const vec3 vect2);

extern float vec3_length(const vec3 vector);
extern float vec3_sq_length(const vec3 vector);

vec3 vec3_unit_vector ( const vec3 vector );

vec3 random_in_unit_disc();
vec3 random_in_unit_disc_multithread(unsigned int *seed);
vec3 random_in_unit_sphere();
vec3 random_in_unit_sphere_multithread(unsigned int *seed);
vec3 random_on_unit_sphere();
vec3 random_on_unit_sphere_multihtread(unsigned int *seed);

int vec3_is_near_zero ( const vec3* v );
vec3 vec3_copy ( const vec3* vector );
vec3 vec3_flip ( const vec3* v1 );

vec3 vec3_random_scaled(float min, float max);
#endif  // VEC3_H
