#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include"vec3.h"

// Utility Functions
inline static float deg2rad(float deg) 
{
    return deg * 3.1415926535897932385f / 180.0f;
}

// random number in [0,1)
inline static float random_float()
{
    return (float) rand() / (RAND_MAX + 1.0f); 
}

// random number for multithreading (without locking)
inline static float random_float_multithread(unsigned int *seed)
{
    return (float) rand_r(seed) / (RAND_MAX + 1.0f);    
}

// random number in [min, max)
inline static float random_float_scaled(float min, float max)
{
    return min + (max-min)*random_float();
}

// multithreaded version
inline static float random_float_scaled_multithread(float min, float max, unsigned int *seed)
{
    return min + (max-min)*random_float_multithread(seed);
}

inline static float clamp(float x, float min, float max)
{
    return (x < min) ? min : ((x > max) ? max : x);
}

#endif // UTILS_H
