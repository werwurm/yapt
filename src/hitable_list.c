#include<stdio.h>
#include "hitable_list.h"
#include "hitable.h"
#include "sphere.h"
#include "AABB.h"
#include "aarect.h"
#include "box.h"
#include "instance.h"

// create and initialize a new list of hitable objects (a.k.a. "world")
hitable_list* hitable_list_new(char* listname)
{
    hitable_list* newlist=NULL;
    
    //allocate memory for list itself
    newlist = (hitable_list*) malloc(sizeof(hitable_list));
    
    // allocate memory for ptrs to objects
    newlist->objects = (hitable**) malloc(sizeof(hitable*));
    
    // newly created list is empty
    newlist->list_size = 0;
    
    newlist->listname = listname;
    
    return newlist;
}

// add existing object to hitable_list, returns index of added object in list
int hitable_list_addObj(hitable_list* the_list, hitable* const newObj)
{
    if((newObj->type == HITABLE_SPHERE) ||
        (newObj->type == HITABLE_MOVING_SPHERE) ||
        (newObj->type == HITABLE_RECT_XY) ||
        (newObj->type == HITABLE_RECT_XZ) ||
        (newObj->type == HITABLE_RECT_YZ) ||
        (newObj->type == HITABLE_BOX) ||
        (newObj->type == HITABLE_TRANSLATE) ||
        (newObj->type == HITABLE_ROTATE) ||
        (newObj->type == HITABLE_CONSTANT_MEDIUM) ||
        (newObj->type == HITABLE_BVH_NODE))
    {
        // resize object list and add object
        the_list->objects = (hitable**) realloc(the_list->objects, sizeof(hitable**) * (the_list->list_size + 1));   
        the_list->objects[the_list->list_size] = newObj;
        the_list->list_size++;        
    }
    else
    {
        printf("Adding objects of type %d not implemented. Aborting!", newObj->type);
        exit(EXIT_FAILURE);
    }
    return (the_list->list_size - 1);   // return index in list
}

// free memory of a list of hitable objects
void hitable_list_free(hitable_list* the_list)
{
    for(unsigned int i=0; i<the_list->list_size; i++)
    {
        hitable_generic_free(the_list->objects[i]);
    }
    free(the_list->objects);
    free(the_list);
}

// test if ray hits an object in the list, return the hit_record next to camera
bool hitable_list_hit(const hitable_list* list, const ray* r, const float t_min, const float t_max, hit_record* rec, unsigned int *seed)
{
    hit_record temp_rec;
    int hit_anything = 0;
    float closest_so_far = t_max;

    //Test all the object between tmin and the current closest hit so far
    for (unsigned int i=0;i<list->list_size;++i){
        hitable* o = list->objects[i];
        if (hitable_object_hit(o, r, t_min, closest_so_far, &temp_rec, seed))
        {
            //Save the hit
            hit_anything = true;
            closest_so_far = temp_rec.t;

            //Copy current record
            *rec = temp_rec;
        }
    }
    return hit_anything;    
} 

// Create bounding volume for hitable_list
bool hitable_list_create_aabb(hitable_list* the_list, float time0, float time1, aabb* output_box)
{
    if(the_list->list_size == 0)
    {
        return false;
    }
    
    aabb temp_box;
    bool first_box = true;
    
    for(unsigned i=0; i< the_list->list_size; i++)      // loop through all objects in list
    {
        hitable* temp_obj = the_list->objects[i];
        // Create bounding box for current object in the_list
        if(!hitable_create_aabb(temp_obj, time0, time1, &temp_box))
        {
            return false;
        }
        // if thats the first box, take it, otherwise create a surrounding box for existing ones
        *output_box = first_box ? temp_box : create_surrounding_box(*output_box, temp_box);
    }
    
    return true;
}
