#include "vec3.h"
#include "utils.h"

inline vec3 vec3_skalarMult(const vec3 self, const float t)
{
    return (vec3) {self.e0 * t, self.e1*t, self.e2*t};
}

inline vec3 vec3_skalarDiv(const vec3 self, const float t)
{
    return (vec3){ self.e0 / t, self.e1 / t, self.e2 /t};
}    

inline vec3 vec3_add(const vec3 vector1, const vec3 vector2)
{
    return (vec3){ vector1.e0 + vector2.e0, vector1.e1 + vector2.e1, vector1.e2 + vector2.e2 };    
}

inline vec3 vec3_sub(const vec3 vector1, const vec3 vector2)
{
    return (vec3) {vector1.e0 - vector2.e0, vector1.e1 - vector2.e1, vector1.e2 - vector2.e2};  
}

inline vec3 vec3_mult(const vec3 vect1, const vec3 vect2)
{
    return (vec3) {vect1.e0 * vect2.e0, vect1.e1 * vect2.e1, vect1.e2 * vect2.e2};
}

inline vec3 vec3_div(const vec3 vect1, const vec3 vect2)
{
    return (vec3) { vect1.e0 / vect2.e0, vect1.e1 / vect2.e1, vect1.e2 / vect2.e2};
}

inline vec3 vec3_cross(const vec3 vect1, const vec3 vect2)
{
    return (vec3) {
       vect1.e1 * vect2.e2 - vect1.e2 * vect2.e1,
       vect1.e2 * vect2.e0 - vect1.e0 * vect2.e2,
       vect1.e0 * vect2.e1 - vect1.e1 * vect2.e0}; 
}

inline float vec3_dot(const vec3 vect1, const vec3 vect2)
{
    return (vect1.e0 * vect2.e0 + vect1.e1 * vect2.e1+ vect1.e2 * vect2.e2);
}

inline float vec3_length(const vec3 vector)
{
    return sqrtf(vector.e0 * vector.e0 + vector.e1 * vector.e1 + vector.e2 * vector.e2);
}

inline float vec3_sq_length(const vec3 vector)
{
    return vector.e0 * vector.e0 + vector.e1 * vector.e1 + vector.e2 * vector.e2;
}

inline vec3 vec3_unit_vector(const vec3 vector)
{
    float length = vec3_length(vector);
    vec3 result = vec3_skalarDiv(vector, length);
    
    return result;
}

vec3 random_in_unit_disc()
{
    vec3 p;
    do
    {
        float random1 = (float)rand() / (float)((unsigned)RAND_MAX + 1);    // create 2 independent
        float random2 = (float)rand() / (float)((unsigned)RAND_MAX + 1);    // random numbers 0 <= x < 1 
        p = (vec3) { random1 * 2.0f, random2 * 2.0f, 0};
        p = vec3_sub(p, (vec3){1,1,0});
    } 
    while (vec3_dot(p,p) >= 1.0f);
        return p;
}

vec3 random_in_unit_disc_multithread(unsigned int *seed)
{
    vec3 p;
    do
    {
        float random1 = (float)rand_r(seed) / (float)((unsigned)RAND_MAX + 1);    // create 2 independent
        float random2 = (float)rand_r(seed) / (float)((unsigned)RAND_MAX + 1);    // random numbers 0 <= x < 1 
        p = (vec3) { random1 * 2.0f, random2 * 2.0f, 0};
        p = vec3_sub(p, (vec3){1,1,0});
    } 
    while (vec3_dot(p,p) >= 1.0f);
        return p;    
}


vec3 random_in_unit_sphere()
{
    vec3 p;
    do
    {
        float random1 = (float)rand() / (float)((unsigned)RAND_MAX + 1);    // create 3 independent
        float random2 = (float)rand() / (float)((unsigned)RAND_MAX + 1);    // random numbers 0 <= x < 1 
        float random3 = (float)rand() / (float)((unsigned)RAND_MAX + 1);
        p = (vec3) { random1 * 2.0f, random2 * 2.0f, random3 * 2.0f};
        p = vec3_sub(p, (vec3){ 1,1,1});
    } 
    while (vec3_sq_length(p) >= 1.0);     // reject all random points that are outside unit sphere
    return p;
}

vec3 random_in_unit_sphere_multithread(unsigned int *seed)
{
    vec3 p;
    do
    {
        float random1 = (float)rand_r(seed) / (float)((unsigned)RAND_MAX + 1);    // create 3 independent
        float random2 = (float)rand_r(seed) / (float)((unsigned)RAND_MAX + 1);    // random numbers 0 <= x < 1 
        float random3 = (float)rand_r(seed) / (float)((unsigned)RAND_MAX + 1);
        p = (vec3) { random1 * 2.0f, random2 * 2.0f, random3 * 2.0f};
        p = vec3_sub(p, (vec3){ 1,1,1});
    } 
    while (vec3_sq_length(p) >= 1.0);     // reject all random points that are outside unit sphere
    return p;
}

vec3 random_on_unit_sphere()
{
    vec3 p;
    do
    {
        float random1 = (float)rand() / (float)((unsigned)RAND_MAX + 1);    // create 3 independent
        float random2 = (float)rand() / (float)((unsigned)RAND_MAX + 1);    // random numbers 0 <= x < 1 
        float random3 = (float)rand() / (float)((unsigned)RAND_MAX + 1);
        p = (vec3) { random1 * 2.0f, random2 * 2.0f, random3 * 2.0f};
        p = vec3_sub(p, (vec3){ 1,1,1});
    } 
    while (vec3_sq_length(p) >= 1.0);     // reject all random points that are outside unit sphere
    
    return vec3_unit_vector(p);    
}

vec3 random_on_unit_sphere_multihtread(unsigned int *seed)
{
    vec3 p;
    do
    {
        float random1 = (float)rand_r(seed) / (float)((unsigned)RAND_MAX + 1);    // create 3 independent
        float random2 = (float)rand_r(seed) / (float)((unsigned)RAND_MAX + 1);    // random numbers 0 <= x < 1 
        float random3 = (float)rand_r(seed) / (float)((unsigned)RAND_MAX + 1);
        p = (vec3) { random1 * 2.0f, random2 * 2.0f, random3 * 2.0f};
        p = vec3_sub(p, (vec3){ 1,1,1});
    } 
    while (vec3_sq_length(p) >= 1.0);     // reject all random points that are outside unit sphere
    
    return vec3_unit_vector(p);    
}

// Check if a vector is near zero
inline int vec3_is_near_zero(const vec3* v)
{
    return (fabs(v->e0) < 1e-8) && (fabs(v->e1) < 1e-8) && (fabs(v->e2) < 1e-8);
}

inline vec3 vec3_copy(const vec3* vector)
{
    vec3 r = {vector->e0, vector->e1, vector->e2};
    return r;
}

inline vec3 vec3_flip(const vec3* v1)
{
    return (vec3){-v1->e0, -v1->e1, -v1->e2};
}

///Create a random vector of min-max components
inline vec3 vec3_random_scaled(float min, float max)
{
    return (vec3){  random_float_scaled(min, max), 
                    random_float_scaled(min, max), 
                    random_float_scaled(min, max)};
}
