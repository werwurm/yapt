CC = clang
CSRCPATH = $(wildcard src/*.c)
OBJPATH = $(addprefix build/,$(notdir $(CSRCPATH:.c=.o)))
LIBS = -lm
CFLAGS = -flto -fopenmp -march=native -O3 -std=gnu99 -Wall -Wextra -Wswitch-enum -fno-strict-aliasing -ffast-math
LDFLAGS= -flto -fopenmp -Wall

.PHONY: all clean debug profile

all:yapt

debug: CFLAGS := $(filter-out -O3, $(CFLAGS))
debug: CFLAGS := $(filter-out -DNDEBUG, $(CFLAGS))
debug: CFLAGS := $(filter-out -ffast-math, $(CFLAGS))
debug: CFLAGS += -DDEBUG -g -O0
debug: yapt

profile: CFLAGS += -pg
profile: LDFLAGS+= -pg
profile: yapt

yapt: $(OBJPATH)
	$(CC) -o $@ $^ $(LIBS) $(LDFLAGS) 

build/%.o: src/%.c
	$(CC) $(CFLAGS) -c $(INCLUDES) -o $@ $<

clean:
	rm -f $(OBJPATH)
	rm -f yapt
