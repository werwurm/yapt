#ifndef SPHERE_H
#define SPHERE_H

#include<stdbool.h>

#include "AABB.h"
#include "vec3.h"
#include "material.h"
#include "hitable.h"
#include "ray.h"

// class definition
typedef struct
{
    point3 center;
    float radius;
    
    struct material* mat;
} sphere;


typedef struct
{
    point3 center0; // center at time1
    point3 center1; // center at time2
    float time0;
    float time1;
    float radius;
    
    struct material* mat;
} moving_sphere;

// constructors
hitable* sphere_init ( point3 center, float radius, material* mat );
hitable* moving_sphere_init ( point3 center0, point3 center1, float radius, float time0, float time1, material* mat);

// destructors
void sphere_free(hitable* object);
void moving_sphere_free(hitable* object);

// bounding boxes
bool sphere_create_aabb(sphere* sp, aabb* output_box);
bool moving_sphere_create_aabb(moving_sphere* sp, float time0, float time1, aabb* output_box);

// ray intersection code
bool sphere_hit(const sphere* sp, const ray *r, float t_min, float t_max, hit_record *rec);
bool moving_sphere_hit(const moving_sphere* sp, const ray *r, float t_min, float t_max, hit_record *rec);

// surface (/texture) coordinate calculation
void sphere_get_uv(const point3* p, float* u, float* v);

// helper functions
point3 moving_sphere_center(const moving_sphere* sp, const float time);

#endif
