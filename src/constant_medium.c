// constant medium density objects, for rendering volumetric effects like smoke, fog, mist
#include <stdio.h>
#include "constant_medium.h"
#include "hitable.h"
#include "vec3.h"

// constructor
hitable* constant_medium_init(hitable* bound, material* mat, float d)
{
    constant_medium* new = (constant_medium*) malloc(sizeof(constant_medium));
    new->boundary = bound;
    new->phase_function = mat;
    new->neg_inv_density = -1/d;
    
    // create generic object
    hitable* object = hitable_generic_init(HITABLE_CONSTANT_MEDIUM, new);
    return object;
}


// destructor
void constant_medium_free(hitable* object)
{
    if(object->type == HITABLE_CONSTANT_MEDIUM)
    {
        // upcast object and call generic destructor for embedded hitable object
        constant_medium* temp = object->obj;
        hitable_generic_free(temp->boundary);
        
        free(temp);
        free(object);
    }
    else
    {
        printf("\ncalling constant_medium_free() for object of type %d. Aborting now\n", object->type);
        exit(EXIT_FAILURE);
    }
}

// bounding boxes
bool constant_medium_create_aabb(constant_medium* obj, float t0, float t1, aabb* the_box)
{
    return hitable_create_aabb(obj->boundary, t0, t1, the_box);
}

// ray intersection code
bool constant_medium_hit(const constant_medium* obj, const ray *r, float t_min, float t_max, hit_record *rec, unsigned int *seed)
{
    hit_record rec1, rec2;
    
    if (!hitable_object_hit(obj->boundary, r, -HUGE_VAL, HUGE_VAL, &rec1, seed))
    {
        return false;
    }
    
    if (!hitable_object_hit(obj->boundary, r, rec1.t+0.0001, HUGE_VAL, &rec2, seed))
    {
        return false;
    }
    
    if (rec1.t < t_min) rec1.t = t_min;
    if (rec2.t > t_max) rec2.t = t_max;
    
    if (rec1.t >= rec2.t) 
    {
        return false;
    }
    
    if (rec1.t < 0)
    {
        rec1.t = 0;
    }
    
    const float ray_length = vec3_length(r->direction);
    const float distance_inside_boundary = (rec2.t - rec1.t) * ray_length;
    const float hit_distance = obj->neg_inv_density * log(random_float_multithread(seed)); // the thicker the medium, the more likely the ray will be scattered
    if(hit_distance > distance_inside_boundary)
    { 
        return false;
    }

    //Register hit
    rec->t = rec1.t + hit_distance / ray_length;
    rec->p = ray_point_at_parameter(*r, rec->t);
    rec->normal = (vec3){1,0,0};
    rec->front_face = 1;
    rec->mat = (struct material*) obj->phase_function;
    return 1;    
    
}
