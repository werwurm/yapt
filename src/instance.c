#include<stdio.h>
#include<stdbool.h>
#include "AABB.h"
#include "instance.h"
#include "utils.h"


static inline void rotatePoint(vec3 original, double sin_theta, double cos_theta, rotation_axis axis, vec3* output)
{
    if (axis == X)
    {
        output->e0 =  cos_theta * original.e0  -  sin_theta * original.e1;
        output->e1 =  sin_theta * original.e0  +  cos_theta * original.e1;
    }
    else if (axis == Y)
    {
        output->e0 =  cos_theta * original.e0  +  sin_theta * original.e2;
        output->e2 = -sin_theta * original.e0  +  cos_theta * original.e2;
    }
    else
    {
        output->e1 =  cos_theta * original.e1  -  sin_theta * original.e2;
        output->e2 =  sin_theta * original.e1  +  cos_theta * original.e2;
    }
}

//#############################################################################
// constructors
//#############################################################################
hitable* instance_translate_init(hitable* obj, vec3 offset)
{   
    instance_translate* new_inst = (instance_translate*) malloc(sizeof(instance_translate));
    new_inst->obj = obj;
    new_inst->offset = offset;
    
    // create generic object
    hitable* object = hitable_generic_init(HITABLE_TRANSLATE, new_inst);

    return object;
}

hitable* instance_rotate_init(hitable* obj, float angle, rotation_axis axis)
{
    instance_rotate* new_inst = (instance_rotate*) malloc(sizeof(instance_rotate));
    new_inst->obj = obj;
    new_inst->axis = axis;
    
    float radians = deg2rad(angle);
    new_inst->sin_theta = sinf(radians);
    new_inst->cos_theta = cosf(radians);
//     new_inst->hasbox = hitable_create_aabb(obj, 0, 1, &new_inst->bbox);
    new_inst->hasbox = instance_rotate_create_aabb(new_inst, &new_inst->bbox);
    // create new AABB
    point3 min = { HUGE_VAL, HUGE_VAL, HUGE_VAL};
    point3 max = { -HUGE_VAL, -HUGE_VAL, HUGE_VAL};
    
    for(unsigned int i=0; i<2; i++) 
    {
        for(unsigned int j=0; j<2; j++) 
        {
            for(unsigned int k=0; k<2; k++) 
            {
                float x = i * new_inst->bbox.maximum.e0 + (1-i) * new_inst->bbox.minimum.e0;
                float y = j * new_inst->bbox.maximum.e1 + (1-j) * new_inst->bbox.minimum.e1;
                float z = k * new_inst->bbox.maximum.e2 + (1-k) * new_inst->bbox.minimum.e2;

                float newx = new_inst->cos_theta * x + new_inst->sin_theta * z;
                float newz = - new_inst->sin_theta *x + new_inst->cos_theta * z;
                vec3 tester = {newx, y, newz};
//                 vec3 tester = p;
//                 rotatePoint(p, new_inst->sin_theta, new_inst->cos_theta, new_inst->axis, &tester);

                min.e0 = fmin(min.e0, tester.e0);
                max.e0 = fmax(max.e0, tester.e0);
                min.e1 = fmin(min.e1, tester.e1);
                max.e1 = fmax(max.e1, tester.e1);
                min.e2 = fmin(min.e2, tester.e2);
                max.e2 = fmax(max.e2, tester.e2);
            }
        }
    }
    new_inst->bbox = (aabb) {min, max};
    
    // create generic object
    hitable* object = hitable_generic_init(HITABLE_ROTATE, new_inst);
    return object;   
}


//#############################################################################
// destructors
//#############################################################################
void instance_translate_free(hitable* object)
{
    if(object->type == HITABLE_TRANSLATE)
    {
        // knot in my brain: upcast object to instance_translate, then call
        // generic free on embedded generic hitable object...
        instance_translate* temp = (instance_translate*) object->obj;
        hitable_generic_free(temp->obj);
        free(temp);
        free(object);
    }
    else
    {
        printf("\ncalling instance_translate_free() for object of type %d. Aborting now\n", object->type);
        exit(EXIT_FAILURE);
    }
}


void instance_rotate_free(hitable* object)
{
    if(object->type == HITABLE_ROTATE)
    {
        // knot in my brain: upcast object to instance_translate, then call
        // generic free on embedded generic hitable object...
        instance_rotate* temp = (instance_rotate*) object->obj;
        hitable_generic_free(temp->obj);

        free(temp);
        free(object);
    }
    else
    {
        printf("\ncalling instance_rotate_free() for object of type %d. Aborting now\n", object->type);
        exit(EXIT_FAILURE);
    }
}    

//#############################################################################
// Bounding Boxes
//#############################################################################
bool instance_translate_create_aabb(instance_translate* obj, float t0, float t1, aabb* the_box)
{
    if(!hitable_create_aabb(obj->obj, t0, t1, the_box))
    {
        return false;
    }
    *the_box = (aabb) { vec3_add(the_box->minimum, obj->offset), vec3_add(the_box->maximum, obj->offset) };
    return true;
}

bool instance_rotate_create_aabb(instance_rotate* obj, aabb* the_box)
{
    *the_box = obj->bbox;
    return obj->hasbox;
}

//#############################################################################
// Ray intersection code
//#############################################################################
bool instance_translate_hit(instance_translate* obj, const ray* r, float tmin, float tmax, hit_record* rec, unsigned int *seed)
{
    // move ray with offset and test on instanced object
    ray moved_ray = { vec3_sub(r->origin, obj->offset), r->direction, r->time};
    if(!hitable_object_hit(obj->obj, &moved_ray, tmin, tmax, rec, seed))
        return false;
    
    // move hit point back
    rec->p = vec3_add(rec->p, obj->offset);
    hit_record_set_facenormal(rec, &moved_ray, &rec->normal);
    return true;
}

bool instance_rotate_hit(instance_rotate* obj, const ray* r, float tmin, float tmax, hit_record* rec, unsigned int *seed)
{
    //Rotate the incoming ray
    point3 origin = r->origin;
    vec3 direction = r->direction;
    rotatePoint(r->origin, -obj->sin_theta, obj->cos_theta, obj->axis, &origin);
    rotatePoint(r->direction, -obj->sin_theta, obj->cos_theta, obj->axis, &direction);
    
    //Test on the wrapped object
    ray rotated_ray = {origin, direction, r->time};
    if (!hitable_object_hit(obj->obj, &rotated_ray, tmin, tmax, rec, seed)) return false;

    //Revert rotation on the normal
    vec3 p = rec->p;
    vec3 normal = rec->normal;

    rotatePoint(rec->p, obj->sin_theta, obj->cos_theta, obj->axis, &p);
    rotatePoint(rec->normal, obj->sin_theta, obj->cos_theta, obj->axis, &normal);
    rec->p = p;
    hit_record_set_facenormal(rec, &rotated_ray, &normal);
    return true;    
}
