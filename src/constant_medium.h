// constant medium density objects, for rendering volumetric effects like smoke, fog, mist

#ifndef CONSTANT_MEDIUM_H
#define CONSTANT_MEDIUM_H

#include<stdbool.h>
#include"vec3.h"
#include"hitable.h"
#include"material.h"

// class definition
typedef struct
{
    hitable* boundary;
    material* phase_function;
    float neg_inv_density;
} constant_medium;

// "constructor"
hitable* constant_medium_init(hitable* bound, material* mat, float d);

// "destructor"
void constant_medium_free(hitable* object);

// bounding boxes
bool constant_medium_create_aabb(constant_medium* obj, float t0, float t1, aabb* the_box);

// ray intersection code
bool constant_medium_hit(const constant_medium* obj, const ray *r, float t_min, float t_max, hit_record *rec, unsigned int *seed);

#endif  // CONSTANT_MEDIUM_H
