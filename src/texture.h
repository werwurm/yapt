#ifndef TEXTURE_H
#define TEXTURE_H

#include"utils.h"
#include"perlin.h"
#include"vec3.h"

typedef enum
{
    TEXTURE_SOLID,
    TEXTURE_CHECKER,
    TEXTURE_NOISE,
    TEXTURE_IMAGE
}texture_type;

// generic texture object
typedef struct
{
    texture_type type;
    void* obj;
} texture;

// texture declarations 
typedef struct      // single, solid color
{
    color color_value;
} texture_solid_color;

typedef struct      // checker texture of 2 colors
{
    texture* odd;
    texture* even;
} texture_checker;

typedef struct      // perlin noise 
{
    perlin* noise;
    float scale;    // scaling factor perlin noise frequency
    color color_value;      // base color of the noise texture
} texture_noise;

typedef struct      // images as texture
{
    unsigned char* data;
    int width;
    int height;
    int bytes_per_scanline;
} texture_image;

// data structure that will hold all textures 
typedef struct
{
    char* listname;
    texture** tex;
    unsigned int list_size;    
} texture_list;

// "constructors"
texture* texture_generic_init(texture_type type, void* obj);
texture* texture_solid_color_init(color col);
texture* texture_solid_color_init_rgb(float r, float g, float b);
texture* texture_checker_init(texture* odd, texture* even);
texture* texture_checker_init_byCol(color odd, color even);
texture* texture_noise_init(color col, float scale);
texture* texture_image_init(char* filename);
texture_list* texture_list_new(char* listname);

// "destructors"
void texture_generic_free(texture* tex);
void texture_solid_color_free(texture_solid_color* tex);
void texture_checker_free(texture_checker* tex);
void texture_noise_free(texture_noise* tex);
void texture_image_free(texture_image* tex);
void texture_list_free(texture_list* the_list);

// texture calculation
color texture_value(texture* tex, float u, float v, const point3* p);
color texture_value_solid_color(texture_solid_color* tex);
color texture_value_checker(texture_checker* tex, float u, float v, const point3* p);
color texture_value_noise(texture_noise* tex, const point3* p);
color texture_value_image(texture_image* tex, float u, float v);

int texture_list_addTex(texture_list* the_list, texture* const newTex);

#endif // TEXTURE_H
