#include<stdio.h>

#include "AABB.h"
#include "sphere.h"
#include "hitable.h"
#include "hitable_list.h"


// create a hitable object of type sphere
hitable* sphere_init(point3 center, float radius, material* mat)
{
    // create sphere
    sphere* sp = malloc(sizeof(sphere));    
    sp->center = center;
    sp->radius = radius; 
    sp->mat = (struct material*) mat;    
    
    // create generic object
    hitable* object = hitable_generic_init(HITABLE_SPHERE, sp);
    return object;
}

// free sphere objects and material attached to it
void sphere_free(hitable* object)
{
    if(object->type == HITABLE_SPHERE)
    {
        // upcast object to sphere type
        sphere* tempsph = object->obj;

        free(tempsph);
        free(object);
    }
    else
    {
        printf("\ncalling sphere_free() for object of type %d. Aborting now\n", object->type);
        exit(EXIT_FAILURE);
    }
}

// ray intersection for spheres, return hit record and true if a sphere is hit
bool sphere_hit(const sphere* sp, const ray *r, float t_min, float t_max, hit_record *rec)
{
    //Find the discriminant from the formula of intersection between a 3d line and a sphere
    vec3 oc = vec3_sub(ray_origin(*r), (sp->center));
    float a = vec3_sq_length(r->direction);
    float half_b = vec3_dot(oc, (r->direction));
    float c = vec3_sq_length(oc) - (sp->radius * sp->radius);
    float discriminant = half_b*half_b - a*c;
    if (discriminant < 0)   // no intersection
    {
        return false;
    }
    
    // calculate quadratic root
    float sqrt = sqrtf(discriminant);
    float root = (-half_b - sqrt) / a;
    if (root < t_min || root > t_max)   // not inside t_min and t_max, check the other root
    {
        root = (-half_b + sqrt) / a;
        if (root < t_min || root > t_max)   // nope, also outside 
        {
            return false;
        }
    }

    rec->t = root;
    rec->p = ray_point_at_parameter(*r, rec->t);
    
    // determine if we are inside or outside sphere
    vec3 outward_normal = vec3_skalarDiv(vec3_sub(rec->p, sp->center), sp->radius);
    hit_record_set_facenormal(rec, r, &outward_normal);  
    
    sphere_get_uv(&outward_normal, &rec->u, &rec->v);
    
    rec->mat = sp->mat;
    return true;    
}

// create a bounding box around sphere
bool sphere_create_aabb(sphere* sp, aabb* output_box)
{
    output_box->minimum = vec3_sub(sp->center, (vec3){sp->radius, sp->radius, sp->radius});
    output_box->maximum = vec3_add(sp->center, (vec3){sp->radius, sp->radius, sp->radius});
    return true;
}

// ===========================================
// moving spheres
// ===========================================
// create a hitable object of type moving sphere
hitable* moving_sphere_init ( point3 center0, point3 center1, float time0, float time1, float radius, material* mat)
{
    // create sphere
    moving_sphere* sp = malloc(sizeof(moving_sphere));    
    sp->center0 = center0;
    sp->center1 = center1;
    sp->time0 = time0;
    sp->time1 = time1;
    sp->radius = radius; 
    sp->mat = (struct material*) mat;    
    
    // create generic object
    hitable* object = hitable_generic_init(HITABLE_MOVING_SPHERE, sp);
    return object; 
}

// free sphere objects and material attached to it
void moving_sphere_free(hitable* object)
{
    if(object->type == HITABLE_MOVING_SPHERE)
    {
        // upcast object to sphere type
        moving_sphere* tempsph = object->obj;

        free(tempsph);
        free(object);
    }
    else
    {
        printf("\ncalling moving_sphere_free() for object of type %d. Aborting now\n", object->type);
        exit(EXIT_FAILURE);
    }
}

// ray intersection for moving spheres, return hit record and true if a moving sphere is hit
bool moving_sphere_hit(const moving_sphere* sp, const ray *r, float t_min, float t_max, hit_record *rec)
{
    //Find the discriminant from the formula of intersection between a 3d line and a sphere
    vec3 tmpcenter = moving_sphere_center(sp, r->time);     // center of sphere at current ray´s time
    vec3 oc = vec3_sub(ray_origin(*r), tmpcenter);
    float a = vec3_sq_length(r->direction);
    float half_b = vec3_dot(oc, (r->direction));
    float c = vec3_sq_length(oc) - (sp->radius * sp->radius);
    float discriminant = half_b*half_b - a*c;
    if (discriminant < 0)   // no intersection
    {
        return false;
    }
    
    // calculate quadratic root
    float sqrt = sqrtf(discriminant);
    float root = (-half_b - sqrt) / a;
    if (root < t_min || root > t_max)   // not inside t_min and t_max, check the other root
    {
        root = (-half_b + sqrt) / a;
        if (root < t_min || root > t_max)   // nope, also outside 
        {
            return false;
        }
    }

    rec->t = root;
    rec->p = ray_point_at_parameter(*r, rec->t);
    
    // determine if we are inside or outside sphere
    vec3 outward_normal = vec3_skalarDiv(vec3_sub(rec->p, tmpcenter), sp->radius);
    hit_record_set_facenormal(rec, r, &outward_normal);    
    
    rec->mat = sp->mat;
    return true;        
}

// center of moving sphere at time t
point3 moving_sphere_center(const moving_sphere* sp, const float time)
{
    point3 tmpcenter;
    tmpcenter = vec3_sub(sp->center1, sp->center0);
    float time_coeff = ( (time - sp->time0) / (sp->time1 - sp->time0) );
    tmpcenter = vec3_skalarMult(tmpcenter, time_coeff);
    
    return (point3) vec3_add((vec3)sp->center0, (vec3)tmpcenter);
}

// create a bounding box around a moving sphere, covering the sphere during whole movement
bool moving_sphere_create_aabb(moving_sphere* sp, float time0, float time1, aabb* output_box)
{
    vec3 area = {sp->radius, sp->radius, sp->radius};
    aabb box0 = {vec3_sub(moving_sphere_center(sp, time0), area), vec3_add(moving_sphere_center(sp, time0), area)};
    aabb box1 = {vec3_sub(moving_sphere_center(sp, time1), area), vec3_add(moving_sphere_center(sp, time1), area)};
    aabb res = create_surrounding_box(box0, box1);
    output_box->minimum = res.minimum;
    output_box->maximum = res.maximum;
    return true;  
    
//     aabb box0 = {vec3_sub(sp->center0, (vec3){sp->radius, sp->radius, sp->radius}), 
//                 vec3_add(sp->center0, (vec3){sp->radius, sp->radius, sp->radius})};
//   
//     aabb box1 = {vec3_sub(sp->center1, (vec3){sp->radius, sp->radius, sp->radius}), 
//                 vec3_add(sp->center1, (vec3){sp->radius, sp->radius, sp->radius})};
//     
//     
//     aabb tmp = create_surrounding_box(box0, box1);
//     
//     output_box->minimum = tmp.minimum;
//     output_box->maximum = tmp.maximum;    
    
    return true;
}

// surface (/texture) coordinate calculation
void sphere_get_uv(const point3* p, float* u, float* v)
{
    float theta = acosf(-p->e1);
    float phi = atan2f(-p->e2, p->e0) + 3.1415926535897932385;
    
    *u = phi / (2* 3.1415926535897932385);
    *v = theta / 3.1415926535897932385;
}
    
