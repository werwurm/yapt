#ifndef BOX_H
#define BOX_H

#include "aarect.h"
#include "hitable.h"
#include "hitable_list.h"
#include "vec3.h"
#include "material.h"

typedef struct
{
    point3 box_min;
    point3 box_max;
    
    hitable_list* sides;
} box;

// constructor
hitable* box_init(point3 p0, point3 p1, material* mat);

// destructor
void box_free(hitable* object);

// bounding box
bool box_create_aabb(box* the_box, aabb* output_box);

// ray intersection
bool box_hit(const box* the_box, const ray *r, float t_min, float t_max, hit_record *rec, unsigned int *seed);

#endif // BOX.H
