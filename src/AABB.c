#include<math.h>
#include "AABB.h"

// create an aabb object
aabb* aabb_init(point3 minimum, point3 maximum)
{
    aabb* newBox = malloc(sizeof(aabb));
    
    newBox->minimum = minimum;
    newBox->maximum = maximum;
    
    return newBox;
}

// swaps value between two floats
void swapValue(float *val1, float *val2)
{
    float temp = *val1;
    *val1 = *val2;
    *val2 = temp;
}

// ray intersection code for axis aligned bounding boxes
bool aabb_hit(const aabb* box, const ray* r, float t_min, float t_max)
{
    // e0 axis
    float invD = 1.0f/(float)r->direction.e0;
    float t0_e0 = (box->minimum.e0 - r->origin.e0) * invD;
    float t1_e0 = (box->maximum.e0 - r->origin.e0) * invD;
    if (invD < 0.0f){swapValue(&t0_e0, &t1_e0);}
    t_min = t0_e0 > t_min ? t0_e0 : t_min;
    t_max = t1_e0 < t_max ? t1_e0 : t_max;
    if (t_max <= t_min) 
    {
        return 0;
    }

    // e1 axis
    invD = 1.0f/(float)r->direction.e1;
    float t0_e1 = (box->minimum.e1 - r->origin.e1)*invD;
    float t1_e1 = (box->maximum.e1 - r->origin.e1)*invD;
    if ( invD < 0.0f){swapValue(&t0_e1, &t1_e1);}
    t_min = t0_e1 > t_min ? t0_e1 : t_min;
    t_max = t1_e1 < t_max ? t1_e1 : t_max;
    if (t_max <= t_min) 
    {
        return 0;
    }

    // e2 axis
    invD = 1.0f/(float)r->direction.e2;
    float t0_e2 = (box->minimum.e2 - r->origin.e2)* invD;
    float t1_e2 = (box->maximum.e2 - r->origin.e2)* invD;
    if (invD < 0.0f)
    {
        swapValue(&t0_e2, &t1_e2);
    }
    t_min = t0_e2 > t_min ? t0_e2 : t_min;
    t_max = t1_e2 < t_max ? t1_e2 : t_max;
    if (t_max <= t_min) 
    {
        return 0;
    }

    //Hit success
    return 1;    
}

// "destructor for bounding boxes)
void aabb_free(aabb* the_box)
{
    free(the_box);
}

// create a bigger box that surrounds both smaller boxes
aabb create_surrounding_box(aabb box0, aabb box1)
{
    point3 small = (point3){fmin(box0.minimum.e0, box1.minimum.e0),
                            fmin(box0.minimum.e1, box1.minimum.e1),
                            fmin(box0.minimum.e2, box1.minimum.e2)};
                            
    point3 big = (point3) {fmax(box0.minimum.e0, box1.minimum.e0),
                            fmax(box0.minimum.e1, box1.minimum.e1),
                            fmax(box0.minimum.e2, box1.minimum.e2)};
                            
    return (aabb){small, big};
}
